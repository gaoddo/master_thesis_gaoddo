#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 09:19:01 2023

@author: gabrieleoddo
"""

# Import modules
from fair.forward import fair_scm
import numpy as np
import pandas as pd 
from matplotlib import pyplot as plt
from scipy import stats

# Climate forcers emissions considered + year
names = ['Year','FossilCO2','OtherCO2','CH4','N2O','SOx','CO','NMVOC','NOx','BC','OC','NH3','CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','CARB_TET','MCF','HCFC_22','HCFC_141B','HCFC_142B','HALON1211','HALON1202','HALON1301','HALON2402','CH3BR','CH3CL']
# Convert from GtC to GtCO2
converter = 44.01/12.011 


def compare_methods(emissions_unchanged, emissions_100, emissions_star, emissions_lwe,
                    T_unchanged, T_100, T_star, T_lwe, T_fict,
                    years, top_xlim=2500):
    """
    Function plotting anthropogenic emissions and temperature increase since the 
    pre-industrial era (1900) for unchanged emissions, GWP100 emissions, GWP* emissions
    and LWE emissions.

    Parameters
    ----------
    emissions_unchanged : 1-D np.array of float
        emissions of all GHG.
    emissions_100 : 1-D np.array of float
        CO2-eq. emissions using GWP100.
    emissions_star : 1-D np.array of float
        CO2-eq. emissions using GWP*.
    emissions_lwe : 1-D np.array of float
        CO2-eq. emissions using LWE.
    T_unchanged : 1-D np.array of float
        temperature evolution since 1900 for unchanged emissions using FAIR.
    T_100 : 1-D np.array of float
        fictive temperature evolution since 1900 using TCRE of GWP100.
    T_star : 1-D np.array of float
        fictive temperature evolution since 1900 using TCRE of GWP*.
    T_lwe : 1-D np.array of float
        fictive temperature evolution since 1900 using TCRE of LWE.
    T_fict : 1-D np.array of float
        fictive temperature evolution since 1900 using TCRE of unchanged emissions.
    years : 1-D np.array of int
        years of the simulation
    top_xlim : int/float, optional
        maximum date on the plot. The default is 2500.

    Returns
    -------
    None.

    """

    fig, ax = plt.subplots(2, 1, figsize=(7,7), dpi=500)
    
    # Anthropogenic CO2 true emissions compared with emissions using different metrics
    ax[0].set_title('a) Cumulative CO$_2$/CO$_2$-eq. emissions')
    ax[0].plot(years, emissions_unchanged*converter, label='CO$_2$ emissions (unchanged)')
    ax[0].plot(years, emissions_100*converter, label='CO$_2$-eq. emissions using GWP$_{100}$')
    ax[0].plot(years, emissions_star*converter, label='CO$_2$-eq. emissions using GWP*')
    ax[0].plot(years, emissions_lwe*converter, label='CO$_2$-eq. emissions using LWE')
    ax[0].grid(True)
    ax[0].set_xticklabels([])
    ax[0].set_ylabel('CO$_2$-equivalent emissions (GtCO$_2$)')
    ax[0].legend(loc='upper left')
    
    # Temperature change since pre-industrial era for each case
    ax[1].set_title('b) Warming from the different carbon budgets')
    ax[1].plot(years, T_fict, label='Expected warming (unchanged)')
    ax[1].plot(years, T_100, label='Expected warming (GWP$_{100}$)')
    ax[1].plot(years, T_star, label='Expected warming (GWP*)')
    ax[1].plot(years, T_lwe, label='Expected warming (LWE)')
    ax[1].plot(years, T_unchanged, label='True warming (FAIR)', color='k')
    ax[1].grid(True)
    ax[1].set_xlabel('Year')
    ax[1].set_ylabel('GSAT change relative to 1900 (°C)')
    ax[1].legend(loc='upper left')
    
    # Set limit for x-axis
    ax[0].set_xlim(1900, top_xlim)
    ax[1].set_xlim(1900, top_xlim)
    
    plt.tight_layout()        
    plt.show(block=False)
    plt.pause(3)
    plt.close(1)
    
    
def EFmod(nyr, a):
    """
    Function to build the Töplitz matrix. Directly taken from the code of
    Allen et al. (2021).

    Parameters
    ----------
    nyr : int
        number of years with emissions considered.
    a : 1-D np.array of float
        vector with different parameters proper to the greehouse gas
        a = [% of lifetime 1, % of lifetime 2, % of lifetime 3, % of lifetime 4,
        lifetime 1, lifetime 2, lifetime 3, lifetime 4,
        convert GtGHG to ppm/ppb, radiative efficiency in W/m2/(ppm/oob)]

    Returns
    -------
    Fcal : 2-D np.array of float
        Töplitz matrix used in the LWE method.

    """

    # Create linear operator to convert emissions to forcing
    Fcal = np.zeros((nyr,nyr))
    # Extend time array to compute derivatives
    time = np.arange(nyr+1)
    # Compute constant term (if there is one, otherwise a[0]=0)
    F_0 = a[0]*a[4]*a[8]*a[9]*time
    
    # Loop over gas decay terms to calculate AGWP using AR5 formula
    for i in [1,2,3]:
        F_0 = F_0+a[i]*a[8]*a[9]*a[i+4]*(1-np.exp(-time/a[i+4]))
        
    # First-difference AGWP to obtain AGFP
    for i in range(0,nyr):
        Fcal[i,0] = F_0[i+1] - F_0[i]
        
    # Build up the rest of the Töplitz matrix
    for i in range(1,nyr):
        Fcal[i:nyr,i] = Fcal[0:nyr-i,0]
    
    return Fcal


def T_from_CO2eq(tcre, cumul_emissions, length, net_neg_emi, shift):
    """
    Function that calculates the warming from CO2-eq. emissions using the TCRE
    and the cumulative emissions from the warming equivalent method used.
    Using the cumulative CO2-eq. emissions and the TCRE from each method, we can
    calculate the warming induced by the scenario using the TCRE formula:
        TCRE = ∆T/∆cumul

    Parameters
    ----------
    tcre : 2-D (1,2) np.array of float
        TCRE of the considered warming equivalent method. Has two values: TCRE
        when considering net positive, respectively negative, CO2 emissions.
    cumul_emissions : 1-D np.array of float
        cumulative CO2 emissions from the unchanged emissions.
    length : int
        length of temperature array
    net_neg_emi : boolean
        net negative CO2 emissions are considered in the scenario when True.
    shift : int
        index where cumulative CO2 emissions shift from warming to cooling.

    Returns
    -------
    T : 1-D np.array of float
        temperature change of the considered warming equivalent method.

    """
    
    T = np.zeros(length)
    if net_neg_emi:
        # Warming using TCRE during net positive CO2 emissions
        T[:shift] = tcre[0]*cumul_emissions[:shift]
        # Warming using TCRE during net negative CO2 emissions
        T[shift:] = np.abs(tcre[1])*cumul_emissions[shift:]
        # Correct the shift due to TCRE change
        T[shift:] += T[shift-1] - T[shift]
    else:
        # Warming using TCRE (no net negative CO2 emissions)
        T = tcre[0]*cumul_emissions   
    return T


def approx(x,slope):
    """
    Function used in vertical_distance.

    Parameters
    ----------
    x : float
        x-value of a scatter point.
    slope : float
        slope of the line.

    Returns
    -------
    float
        x-value of the point on the given slope.

    """
    return slope*x


def vertical_distance(point, approx, slope):
    """
    Function calculating the vertical distance betweeen a point a line with a given 
    slope.

    Parameters
    ----------
    point : 1-D np.array of float
        coordinates of the considered point.
    approx : float
        x-value of the point on the given slope.
    slope : float
        slope of the line representing the TCRE.

    Returns
    -------
    distance : float
        vertical distance between the considered point and the line with a given slope.

    """
    x_point, y_point = point
    y_curve = approx(x_point, slope)
    distance = y_point - y_curve
    
    return distance

def horizontal_distance(point, approx, slope):
    """
    Function calculating the vertical distance betweeen a point a line with a given 
    slope.

    Parameters
    ----------
    point : 1-D np.array of float
        coordinates of the considered point.
    approx : float
        x-value of the point on the given slope.
    slope : float
        slope of the line representing the TCRE.

    Returns
    -------
    distance : float
        vertical distance between the considered point and the line with a given slope.

    """
    x_point, y_point = point
    x_curve = approx(y_point, slope)
    distance = x_point - x_curve
    
    return distance


def tcre_calculation(TC, extrema):
    """
    Function computing the TCRE for multiple percentiles based a range of possible slopes. 
    The linear regressions must intercept the point (0,0). This TCRE is based on the 
    temperature change and the cumulative CO2 emissions of each scenario. This can be done
    both with true CO2 emissions or CO2-equivalent emissions using one of the different 
    methods.

    Parameters
    ----------
    TC : 2-D np.array of float
        temperature change and cumulative CO2(-eq.) emissions of each considered scenario.
    extrema : 1-D np.array of float
        low, respectively high, TCRE range (based on observations).

    Returns
    -------
    tcre_quantile : 1-D pd.DataFrame
        TCRE value for each percentile.

    """
    
    # Range of possible linear regressions
    slopes = np.arange(extrema[0],extrema[1],0.001)
    
    quantile_per_slope = np.zeros(len(slopes))
    quantiles = np.arange(0.05,1,0.05)
    converter = 44.01/12.011 

    for i in range(len(slopes)):
        distance = np.zeros(len(TC))
        for j in range(len(TC)):
            point_scatter = (TC[j,0]*converter,TC[j,1])
            # Calculate vertical distance between point and slope
            distance[j] = vertical_distance(point_scatter, approx, slopes[i]/1000)
        # Percentile of the selected linear regression
        quantile_per_slope[i] = np.sum(distance < 0, axis=0)/len(TC)

    tcre_quantile = np.zeros(len(quantiles))
    for i in range(len(tcre_quantile)):
        # Select linear regression lines corresponding to the given quantiles
        tcre_quantile[i] = slopes[np.argmax(quantile_per_slope > quantiles[i])]
    
    # Convert tcre_quantile into DataFrame
    tcre_quantile = pd.DataFrame(tcre_quantile.reshape(len(tcre_quantile),1).T,columns=[np.round(quantiles,2).astype('str')])
        
    return tcre_quantile

def relative_error(X_fair, X_method):
    """
    Function calculating the distance between each each point and y=x line. This 
    gives the relative error between the warming (or RGCB) from FAIR and the 
    fictive warming from a non-CO2 warming-equivalent method.

    Parameters
    ----------
    X_fair : 2-D np.array of float
        warming relative to 1900 of all scenarios using FAIR starting in year 2015.
    X_method : TYPE
        warming relative to 1900 of all scenarios using a method sarting in year 2015.

    Returns
    -------
    err : 2-D np.array of float
        distance between each point and y=x line.

    """
    
    if len(np.shape(X_fair)) == 2:
        err = np.zeros((len(X_fair),len(X_fair[0])))
        for i in range(len(X_fair)):
            for j in range(len(X_fair[0])):
                point = (X_fair[i,j], X_method[i,j])
                x_dist = horizontal_distance(point, approx, 1)
                y_dist = vertical_distance(point, approx, 1)
                err[i,j] = np.sqrt(x_dist**2 + y_dist**2)
    else:
        err = np.zeros(len(X_fair))
        for i in range(len(X_fair)):
            point = (X_fair[i], X_method[i])
            x_dist = horizontal_distance(point, approx, 1)
            y_dist = vertical_distance(point, approx, 1)
            err[i] = np.sqrt(x_dist**2 + y_dist**2)
    
    return err


def ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist):
    """
    Function giving a random set of TCRE values from a range of possible TCREs and
    the likelihood of the actual TCRE being within that range. This function comes 
    directly from the AR6 IPCC code.

    Parameters
    ----------
    low : float
        The lower limit of the probability distribution.
    high : float
        The upper limit of the probability distribution. Must be larger than low. 
    likelihood : float
        Probability (between 0 and 1) that the value will be between low and high.
    n_return : int
        The number of values to return.
    tcre_dist : str
        Either "normal" for a normal distribution, "lognormal" for a lognormal
        distribution, or "lognormal mean match", for a lognormal but with the same mean
        and standard deviation as a normal distribution fitting the same
        high/low/likelihood values.

    Raises
    ------
    ValueError
        if tcre_dist is neither "normal", "lognormal" nor "lognormal mean match".

    Returns
    -------
    1-D np.array of float
        random set of values distributed according to tcre_dist.

    """
    
    assert high > low, "High and low limits are the wrong way around"
    if tcre_dist == "normal":
        # We want a normal distribution such that we are between low and high with a
        # given likelihood.
        mean = (high + low) / 2
        # find the normalised z score that gives a normal cdf with given likelihood of
        # being between the required high and low values
        z = stats.norm.ppf((1 + likelihood) / 2)
        sd = (high - mean) / z
        return np.random.normal(mean, sd, n_return)
    elif tcre_dist == "lognormal mean match":
        mean = (high + low) / 2
        assert mean > 0, "lognormal distributions are always positive"
        z = stats.norm.ppf((1 + likelihood) / 2)
        sd = (high - mean) / z
        # The lognormal function takes arguments of the underlying mu and sigma values,
        # which are not the same as the actual mean and s.d., so we convert below
        # Derive relations from: mean = exp(\mu + \sigma^2/2),
        # sd = (exp(\sigma^2) - 1)^0.5 * exp(\mu + \sigma^2/2)
        sigma = (np.log(1 + (sd / mean) ** 2)) ** 0.5
        mu = np.log(mean) - sigma ** 2 / 2
        return np.random.lognormal(mean=mu, sigma=sigma, size=n_return)
    elif tcre_dist == "lognormal":
        assert high > 0
        assert low > 0
        # We have h = exp(\mu + \sigma z), l = exp(\mu - \sigma z) ,
        # for z normally distributed as before. Rearranging and solving:
        z = stats.norm.ppf((1 + likelihood) / 2)
        mu = 0.5 * np.log(low * high)
        sigma = 0.5 * np.log(high / low)
        return np.random.lognormal(mean=mu, sigma=sigma, size=n_return)
    # If you haven't returned yet, something went wrong.
    raise ValueError(
        "tcre_dist must be either normal, lognormal mean match or lognormal, it was {}".format(
            tcre_dist
        )
    )

def ipcc_RGCB(dT_target, zec, historical_dT, tcre, earth_feedback_co2):
    """
    Function calculating the remaining global carbon budget based on the parameters it depends
    on. It can both consider unchanged or CO2-eq.-converted emissions. The impact of 
    non-CO2 contribution is directly contained in the TCRE.

    Parameters
    ----------
    dT_target : float
        The target temperature change to achieve.
    zec : float
        The change in temperature that will occur after zero emissions has been reached.
    historical_dT : float
        The mean global air surface temperature difference already seen since the pre-industrial
        period.
    tcre : 1-D np.array of float
        Distribution of transient climate response to cumulative CO2 or CO2-equivalent emissions
        (see ipcc_tcre_distribution function).
        
    earth_feedback_co2 : float
        CO2 emissions from temperature-dependent Earth feedback loops.

    Returns
    -------
    remaining_dT : float
        remaining global CO2 or CO2-equivalent budget.

    """

    remaining_dT = (dT_target - zec - historical_dT)/tcre - earth_feedback_co2
    
    return remaining_dT

def ipcc_EF(dtemp, co2_per_degree_av, co2_per_degree_stdv, nloops):
    """
    Function calculating different possible amounts of CO2 coming from Earth systems feedback 
    based on the average and standard deviation of such feedback. Earth systems feedback depends
    on the warming reached.

    Parameters
    ----------
    dtemp : float
        The additional warming expected.
    co2_per_degree_av : float
        The average of amount of CO2 emitted per additional degree of warming.
    co2_per_degree_stdv : float
        The standard deviation of amount of CO2 emitted per additional degree of warming.
    nloops : int
        The number of values to return.

    Returns
    -------
    earth_feedback_co2 : 1-D np.array of float
        CO2 emitted by earth systems feedback.

    """
    
    earth_feedback_co2 = dtemp * np.random.normal(co2_per_degree_av, co2_per_degree_stdv, nloops)
    
    return earth_feedback_co2



