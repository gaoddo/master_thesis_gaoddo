#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 17:01:13 2023

@author: gabrieleoddo
"""

# Import modules
import numpy as np
import pandas as pd

##### Directory
path = '/Users/gabrieleoddo/Documents/ETHZ/Master_Thesis/FaIR_Gabriele/'

#%%
###############################################################################
################################ Data formating ###############################
###############################################################################
# Load world emissions data and keep only year 2021
# From https://data.worldbank.org/indicator/SP.POP.TOTL?end=2021&start=1960
total_emissions = pd.read_csv(path+'total-ghg-emissions.csv') # in tCO2-eq. (GWP100)
# Change name of column with emissions
total_emissions = total_emissions.rename(columns={"Annual greenhouse gas emissions in CO2 equivalents": "Emission"})
# Select only 2021 values
total_emissions21 = total_emissions[total_emissions.Year == 2021].reset_index(drop=True)
total_emissions21 = total_emissions21.dropna(axis=0).reset_index(drop=True)
countries_emi = np.array(total_emissions21.Code.unique())
# Select only 2015 values
total_emissions15 = total_emissions[total_emissions.Year == 2015].reset_index(drop=True)
total_emissions15 = total_emissions15.dropna(axis=0).reset_index(drop=True)
# Select only 1998 values
total_emissions97 = total_emissions[total_emissions.Year == 1997].reset_index(drop=True)
total_emissions97 = total_emissions97.dropna(axis=0).reset_index(drop=True)

# Load world population data and keep countries contained in total_emissions
# From https://ourworldindata.org/population-growth
total_population = pd.read_csv(path+'total-population.csv',sep=';')
total_population = total_population[total_population['Country Code'].isin(countries_emi)].reset_index(drop=True)
countries_pop = np.array(total_population['Country Code'].unique())

# Delete countries in total_emissions not contained in total_population
delete_list = list(set(countries_emi)-set(countries_pop))
total_emissions21 = total_emissions21[~total_emissions21.Code.isin(delete_list)].reset_index(drop=True)
total_emissions15 = total_emissions15[~total_emissions15.Code.isin(delete_list)].reset_index(drop=True)
total_emissions97 = total_emissions97[~total_emissions97.Code.isin(delete_list)].reset_index(drop=True)

# Sort both DataFrames by country
total_emissions21 = total_emissions21.sort_values(by='Code').reset_index(drop=True)
total_emissions15 = total_emissions15.sort_values(by='Code').reset_index(drop=True)
total_emissions97 = total_emissions97.sort_values(by='Code').reset_index(drop=True)
total_population = total_population.sort_values(by='Country Code').reset_index(drop=True)

#%%
###############################################################################
################### Swiss remaining national carbon budget ####################
###############################################################################
# =============================================================================
# Share of RGCB for Switzerland based on Bretscher Policy Brief
# =============================================================================
# Fairness index of each country
fairness_index = np.array((total_emissions21.Emission/total_population['2021'])**0.25)

# Fraction of the population of each country over whole globe
fract_pop = np.array(total_population['2021'])/total_population['2021'].sum()

# Values for Switzerland
F_index_CH = fairness_index[np.where(total_emissions21.Code == 'CHE')[0][0]]
fract_pop_CH = fract_pop[np.where(total_emissions21.Code == 'CHE')[0][0]]

# Swiss share of RGCB with B. Policy Brief
share_bpb_CH = fract_pop_CH*F_index_CH/np.sum(fract_pop*fairness_index)

# =============================================================================
# Parameters for the national budget
# =============================================================================
##### Swiss share of RGCB with two other allocation mechanisms
# Equal-per-capita (each individual gets same amount of emissions)
share_epc_CH = 0.1100/100
# Per-capita-convergence or grandfathering (based on current total emissions)
share_pcc_CH = 0.0829/100

##### Adjustements for the countries budget
# Land uptake (15% less)
adj_land_uptake = 0.85
# Emissions from aviation and shipping (not included in national emissions)
adj_avi_ship = 46 # GtCO2
# Swiss GHG emissions from January 1 2020 onwards (2020 and 2021)
ref_emi_CO2 = 34.93 # year 2021 (ourworldindata)
ref_emi_CO2eq = 45.25 # year 2021
adj_emi_CO2 = 34.24 + ref_emi_CO2 # MtCO2 
adj_emi_CO2eq = 43.91 + ref_emi_CO2eq # MtCO2-eq. (GWP100)

##### RGCB computed in calculate_RGCB.py (only with FAIR and LWE) consistent with IPCC
# The values are rgcb15_fair_nor and rgcb_lwe_nor, respectively, in MtCO2
rgcb15_fair = 464006.11 
rgcb15_lwe = 653165.55

# =============================================================================
# Calculation of the Swiss national budget
# =============================================================================
##### Swiss remaining carbon budget
# 0: epc, 1: pcc, 2: bpb
rCHcb15_fair = np.zeros(3)
rCHcb15_lwe = np.zeros(3)

# For CO2 emissions
rCHcb15_fair[0] = share_epc_CH*(rgcb15_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_CO2
rCHcb15_fair[1] = share_pcc_CH*(rgcb15_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_CO2
rCHcb15_fair[2] = share_bpb_CH*(rgcb15_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_CO2

# For CO2-eq. emissions (using LWE)
rCHcb15_lwe[0] = share_epc_CH*(rgcb15_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_CO2eq
rCHcb15_lwe[1] = share_pcc_CH*(rgcb15_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_CO2eq
rCHcb15_lwe[2] = share_bpb_CH*(rgcb15_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_CO2eq

# =============================================================================
# Swiss mitigation pathways
# =============================================================================
##### Straight line pathway (slp): calculate year when net 0 CO2 emissions are reached
year_of_net0_slp_fair = (rCHcb15_fair*2/ref_emi_CO2 + 2021).astype(int)
year_of_net0_slp_lwe = (rCHcb15_lwe*2/ref_emi_CO2eq + 2021).astype(int)

# Swiss emissions in 1990
base_emi_CO2 = 44.16 # MtCO2 (ourworldindata)
base_emi_CO2eq = 55.34 # MtCO2eq (GWP100)

# The two other pathways aim to reach net 0 in 2050, with intermediate objectives in 2030 and 2040.
##### Net 0 at 2050 pathway 1: 34% reduction in 2030, 75% reduction in 2040 (relative to 1990)
budget_net0_p1_fair = 0.5*(8*(ref_emi_CO2 - base_emi_CO2*0.66) + 10*base_emi_CO2*0.41 + 10*base_emi_CO2*0.25) + 8*base_emi_CO2*0.66 + 10*base_emi_CO2*0.25
budget_net0_p1_lwe = 0.5*(8*(ref_emi_CO2eq - base_emi_CO2eq*0.66) + 10*base_emi_CO2eq*0.41 + 10*base_emi_CO2eq*0.25) + 8*base_emi_CO2eq*0.66 + 10*base_emi_CO2eq*0.25

##### Net 0 at 2050 pathway 2: 50% reduction in 2030, 75% reduction in 2040 (relative to 1990)
budget_net0_p2_fair = 0.5*(8*(ref_emi_CO2 - base_emi_CO2*0.5) + 20*base_emi_CO2*0.5) + 8*base_emi_CO2*0.5
budget_net0_p2_lwe = 0.5*(8*(ref_emi_CO2eq - base_emi_CO2eq*0.5) + 20*base_emi_CO2eq*0.5) + 8*base_emi_CO2eq*0.5

##### Year when slp surpassed: need to solve a system
# Done for all allocation mechanisms
surp_year_path1_fair = np.zeros(3)
surp_year_path2_fair = np.zeros(3)
surp_year_path1_lwe = np.zeros(3)
surp_year_path2_lwe = np.zeros(3)

for i in range(3):
    # For net 0 pathway 1 and CO2 emissions
    C = -rCHcb15_fair[i]+4*(ref_emi_CO2*0.66)+8*base_emi_CO2*0.66-1015*base_emi_CO2*0.66-1015*base_emi_CO2**2*(0.66-203*(0.66-0.25))
    B = base_emi_CO2/2*0.66+base_emi_CO2**2/2*0.66-base_emi_CO2**2*203*(0.66-0.25)
    A = base_emi_CO2**2/20*(0.66-0.25)
    disc = np.sqrt(B**2-4*A*C)
    y1 = (-B+disc)/(2*A)
    y2 = (-B-disc)/(2*A)
    surp_year_path1_fair[i] = y1
    
    # For net 0 pathway 1 and CO2-eq. emissions
    C = -rCHcb15_lwe[i]+4*(ref_emi_CO2eq*0.66)+8*base_emi_CO2eq*0.66-1015*base_emi_CO2eq*0.66-1015*base_emi_CO2eq**2*(0.66-203*(0.66-0.25))
    B = base_emi_CO2eq/2*0.66+base_emi_CO2eq**2/2*0.66-base_emi_CO2eq**2*203*(0.66-0.25)
    A = base_emi_CO2eq**2/20*(0.66-0.25)
    disc = np.sqrt(B**2-4*A*C)
    y1 = (-B+disc)/(2*A)
    y2 = (-B-disc)/(2*A)
    surp_year_path1_lwe[i] = y1

    # For net 0 pathway 2 and CO2 emissions
    C = -rCHcb15_fair[i]+4*(ref_emi_CO2*0.5)+8*base_emi_CO2*0.5-1015*base_emi_CO2*0.5-1015*base_emi_CO2**2*(0.5-203*(0.5-0.25))
    B = base_emi_CO2/2*0.5+base_emi_CO2**2/2*0.5-base_emi_CO2**2*203*(0.5-0.25)
    A = base_emi_CO2**2/20*(0.5-0.25)
    disc = np.sqrt(B**2-4*A*C)
    y1 = (-B+disc)/(2*A)
    y2 = (-B-disc)/(2*A)
    surp_year_path2_fair[i] = y1
    
    # For net 0 pathway 2 and CO2-eq. emissions
    C = -rCHcb15_lwe[i]+4*(ref_emi_CO2eq*0.5)+8*base_emi_CO2eq*0.5-1015*base_emi_CO2eq*0.5-1015*base_emi_CO2eq**2*(0.5-203*(0.5-0.25))
    B = base_emi_CO2eq/2*0.5+base_emi_CO2eq**2/2*0.5-base_emi_CO2eq**2*203*(0.5-0.25)
    A = base_emi_CO2eq**2/20*(0.5-0.25)
    disc = np.sqrt(B**2-4*A*C)
    y1 = (-B+disc)/(2*A)
    y2 = (-B-disc)/(2*A)
    surp_year_path2_lwe[i] = y1

#%%
###############################################################################
####### Same calculations for RGCB starting at Paris agreement year 2016 ######
###############################################################################
# =============================================================================
# Share of RGCB for Switzerland based on Bretscher Policy Brief
# =============================================================================
# Fairness index of each country
fairness_index_Paris = np.array((total_emissions15.Emission/total_population['2015'])**0.25)

# Fraction of the population of each country over whole globe
fract_pop_Paris = np.array(total_population['2015'])/total_population['2015'].sum()

# Values for Switzerland
F_index_CH_Paris = fairness_index_Paris[np.where(total_emissions15.Code == 'CHE')[0][0]]
fract_pop_CH_Paris = fract_pop_Paris[np.where(total_emissions15.Code == 'CHE')[0][0]]

# Swiss share of RGCB with B. Policy Brief
share_bpb_CH_Paris = fract_pop_CH_Paris*F_index_CH_Paris/np.sum(fract_pop_Paris*fairness_index_Paris)

# =============================================================================
# Parameters for the national budget
# =============================================================================
##### Swiss share of RGCB with two other allocation mechanisms
# Equal-per-capita (each individual gets same amount of emissions)
share_epc_CH_Paris = 0.1115/100
# Per-capita-convergence or grandfathering (based on current total emissions)
share_pcc_CH_Paris = 0.0914/100

##### RGCB computed in calculate_RGCB.py (only with FAIR and LWE) starting after Paris agreement
rgcbParis_fair = 879319.371
rgcbParis_lwe = 1246244.053

# Addition of Swiss emissions from 2016 to 2021
adj_emi_Paris_CO2 = 39.19 + 38.18 + 36.87 + 36.73 + 34.24 + ref_emi_CO2 # MtCO2 
adj_emi_Paris_CO2eq = 49.34 + 48.37 + 46.86 + 46.59 + 43.91 + ref_emi_CO2eq # MtCO2-eq. (GWP100)

##### Swiss remaining carbon budget
# 0: epc, 1: pcc, 2: bpb
rCHcbParis_fair = np.zeros(3)
rCHcbParis_lwe = np.zeros(3)

# For CO2 emissions
rCHcbParis_fair[0] = share_epc_CH*(rgcbParis_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Paris_CO2
rCHcbParis_fair[1] = share_pcc_CH*(rgcbParis_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Paris_CO2
rCHcbParis_fair[2] = share_bpb_CH*(rgcbParis_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Paris_CO2

# For CO2-eq. emissions (using LWE)
rCHcbParis_lwe[0] = share_epc_CH*(rgcbParis_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Paris_CO2eq
rCHcbParis_lwe[1] = share_pcc_CH*(rgcbParis_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Paris_CO2eq
rCHcbParis_lwe[2] = share_bpb_CH*(rgcbParis_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Paris_CO2eq

##### Straight line pathway (slp): calculate year when net 0 reached
year_of_net0_slp_Paris_fair = (rCHcbParis_fair*2/ref_emi_CO2 + 2021).astype(int)
year_of_net0_slp_Paris_lwe = (rCHcbParis_lwe*2/ref_emi_CO2eq + 2021).astype(int)

#%%
###############################################################################
####### Same calculations for RGCB starting at Kyoto Protocol year 1998 #######
###############################################################################
# =============================================================================
# Share of RGCB for Switzerland based on Bretscher Policy Brief
# =============================================================================
# Fairness index of each country
fairness_index_Kyoto = np.array((total_emissions97.Emission/total_population['1997'])**0.25)

# Fraction of the population of each country over whole globe
fract_pop_Kyoto = np.array(total_population['1997'])/total_population['1997'].sum()

# Values for Switzerland
F_index_CH_Kyoto = fairness_index_Kyoto[np.where(total_emissions97.Code == 'CHE')[0][0]]
fract_pop_CH_Kyoto = fract_pop_Kyoto[np.where(total_emissions97.Code == 'CHE')[0][0]]

# Swiss share of RGCB with B. Policy Brief
share_bpb_CH_Kyoto = fract_pop_CH_Kyoto*F_index_CH_Kyoto/np.sum(fract_pop_Kyoto*fairness_index_Kyoto)

# =============================================================================
# Parameters for the national budget
# =============================================================================
##### Swiss share of RGCB with two other allocation mechanisms
# Equal-per-capita (each individual gets same amount of emissions)
share_epc_CH_Kyoto = 0.1188/100
# Per-capita-convergence or grandfathering (based on current total emissions)
share_pcc_CH_Kyoto = 0.1311/100

# =============================================================================
# Same calculations for RGCB starting at Kyoto Protocol year 1998
# =============================================================================
##### RGCB computed in calculate_RGCB.py (only with FAIR and LWE) starting after Kyoto Protocol
rgcbKyoto_fair = 1554236.016 # MtCO2
rgcbKyoto_lwe = 2191631.258 # MtCO2-eq.

# Addition of Swiss emissions from 1998 to 2021
adj_emi_Kyoto_CO2 = 44.62+44.45+43.62+45.08+43.46+44.65+45.24+45.78+45.37+43.37+44.71+43.54+45.05+40.99+42.25+43.19+39.23+38.73+ 39.19+38.18+36.87+36.73+34.24+ref_emi_CO2 # MtCO2 
adj_emi_Kyoto_CO2eq = 55.34+54.83+54.07+55.58+54.01+55.06+55.64+56.25+55.92+53.98+55.36+53.87+55.46+51.33+52.66+53.49+49.55+49.03+49.34+48.37+46.86+46.59+43.91+ref_emi_CO2eq # MtCO2-eq. (GWP100)

##### Swiss emaining carbon budget
# 0: epc, 1: pcc, 2: bpb
rCHcbKyoto_fair = np.zeros(3)
rCHcbKyoto_lwe = np.zeros(3)

# For CO2 emissions
rCHcbKyoto_fair[0] = share_epc_CH_Kyoto*(rgcbKyoto_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Kyoto_CO2
rCHcbKyoto_fair[1] = share_pcc_CH_Kyoto*(rgcbKyoto_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Kyoto_CO2
rCHcbKyoto_fair[2] = share_bpb_CH_Kyoto*(rgcbKyoto_fair*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Kyoto_CO2

# For CO2-eq. emissions (using LWE)
rCHcbKyoto_lwe[0] = share_epc_CH_Kyoto*(rgcbKyoto_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Kyoto_CO2eq
rCHcbKyoto_lwe[1] = share_pcc_CH_Kyoto*(rgcbKyoto_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Kyoto_CO2eq
rCHcbKyoto_lwe[2] = share_bpb_CH_Kyoto*(rgcbKyoto_lwe*adj_land_uptake - adj_avi_ship*1e-3) - adj_emi_Kyoto_CO2eq

##### Straight line pathway (slp): calculate year when net 0 reached
year_of_net0_slp_Kyoto_fair = (rCHcbKyoto_fair*2/ref_emi_CO2 + 2021).astype(int)
year_of_net0_slp_Kyoto_lwe = (rCHcbKyoto_lwe*2/ref_emi_CO2eq + 2021).astype(int)


