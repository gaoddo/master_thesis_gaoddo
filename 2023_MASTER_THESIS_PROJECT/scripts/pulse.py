#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 10:28:32 2023

@author: gabrieleoddo
"""

# Import modules

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

from fair.ancil import natural, cmip6_volcanic, cmip6_solar

# Import functions
from thesis_functions import EFmod

#%%
###############################################################################
########################## Initial variables and data #########################
###############################################################################

##### Directory
path = '/Users/gabrieleoddo/Documents/ETHZ/Master_Thesis/FaIR_Gabriele/'

##### Lists of names with the climate forcers and pathways
# Different cases
case = ['True','GWP100','GWPstar','LWE']
# Climate forcers emissions considered + year
names = ['Year','FossilCO2','OtherCO2','CH4','N2O','SOx','CO','NMVOC','NOx','BC','OC','NH3','CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','CARB_TET','MCF','HCFC_22','HCFC_141B','HCFC_142B','HALON1211','HALON1202','HALON1301','HALON2402','CH3BR','CH3CL']
# Non-greenhouse gases
non_ghg_names = ['SOx','NMVOC','NOx','BC','OC','NH3', 'CO']
# Greenhouse gases (non-CO2)
other_ghg_names = sorted(list(set(names[3:]) - set(non_ghg_names)))
# Indirect greenhouse gases
indirect_ghg = ['BC','CO','NH3','NMVOC','NOx']
# Cooling impact climate forcers
neg_gwp_gases = ['OC','SOx']
# F-gases names
F_gases = ['CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','HCFC_22','HCFC_141B','HCFC_142B']
# Socioeconomic pathways considered
ssp = ['ssp119','ssp126','ssp245','ssp370','ssp434','ssp460','ssp534','ssp585']

##### Unit conversions
# Exact unit
units = ['','GtC/yr','GtC/yr','MtCH4/yr','MtN2O-N/yr','MtS/yr','MtCO/yr','Mt/yr','MtN/yr','Mt/yr','Mt/yr','MtN/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr']
# Convert in t of emisssion
unit_ton = [1e9,1e9,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3]
# Convert from GtC to GtCO2
converter = 44.01/12.011 

##### Variables related to remaining global carbon budget
# Natural CH4 and N2O
naturalEmissions = natural.Emissions.emissions
# Zero emissions coefficients
zeroEmissions = np.array([0, 0, 0, 18.6992, 2.16883, 6.504991805, 0, 0, 0, 0, 0, 0, 0.0107, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 155.925, 3511.082])	
# Natural radiative forcing
F_solar = cmip6_solar.Forcing.solar
F_volcanic = cmip6_volcanic.Forcing.volcanic

###### Parameters for the different methods
# Coefficients of the GWP100
gwp100 = [1,1,27.9,273,-133.14,5.3,4.5,-11,900,-69,2.16,7380,12400,8620,14600,771,1600,3740,1530,5810,3600,962,25200,5560,11200,6520,9430,9600,2200,161,1960,860,2300,1930,216,7200,2170,2.43,5.54]
gwp100_ghg_only = [1,1,27.9,273,0,0,0,0,0,0,0,7380,12400,8620,14600,771,1600,3740,1530,5810,3600,962,25200,5560,11200,6520,9430,9600,2200,161,1960,860,2300,1930,216,7200,2170,2.43,5.54]

# Coefficients for the GWP* metric (optimized for CH4)
a = 4.53
b = 4.25
# Import parameters for the LWE method respective to each greenhouse gas
from lwe_parameters import a_ghg

#%%
###############################################################################
######################### Creation of special scenarios #######################
###############################################################################

# Adapt parameters for such scenario
yr_num = 140
years = np.arange(2000,2000+yr_num)

F_volcanic = F_volcanic[:yr_num]
F_solar = F_solar[:yr_num]

# =============================================================================
# Temperature change for one gas
# =============================================================================
def ETmod(nyr,a,a_prime):
    Tcal=np.zeros((nyr,nyr))   # create linear operator to convert emissions to warming
    time=np.arange(nyr)+1      # add one to the time array for consistency with AR5 formulae
    for j in [0,1]:            # loop over thermal response times using AR5 formula for AGTP
        Tcal[:,0]=Tcal[:,0]+a[0]*a[8]*a[9]*a_prime[j]*(1-np.exp(-time/a_prime[j+2]))
        for i in [1,2,3]:      # loop over gas decay terms using AR5 formula for AGTP
            Tcal[:,0]=Tcal[:,0]+a[8]*a[9]*a[i]*a[i+4]*a_prime[j]*(np.exp(-time/a[i+4])-np.exp(-time/a_prime[j+2]))/(a[i+4]-a_prime[j+2])      
    for j in range(1,nyr):     # build up the rest of the Toeplitz matrix
        Tcal[j:nyr,j]=Tcal[0:nyr-j,0]
    return Tcal

a_prime = np.array([0.631*0.7,0.429*0.7,8.400,409.5])
Tch4 = ETmod(yr_num,a_ghg[names.index('CH4')-2],a_prime)
Tn2o = ETmod(yr_num,a_ghg[names.index('N2O')-2],a_prime)
E1 = np.zeros([yr_num,2])
E1[0,0] = 1.
E1[:,1] = np.ones(yr_num)
T_only_CH4 = Tch4@E1
T_only_N2O = Tn2o@E1

# =============================================================================
# Pulse emissions
# =============================================================================
##### Unchanged emissions
# Methane only
pulse_CH4_fair = pd.DataFrame(0,index=range(len(years)),columns=[names])
pulse_CH4_fair.Year = years
pulse_CH4_fair.loc[0,('CH4')] = 1

# Nitrous oxide only
pulse_N2O_fair = pd.DataFrame(0,index=range(len(years)),columns=[names])
pulse_N2O_fair.Year = years
pulse_N2O_fair.loc[0,('N2O')] = 1

##### Converted emissions
# GWP100
pulse_CH4_100 = np.array(pulse_CH4_fair)[:,3]*gwp100[2]*unit_ton[2]/converter*1e-9
pulse_N2O_100 = np.array(pulse_N2O_fair)[:,4]*gwp100[3]*unit_ton[3]/converter*1e-9

# GWP*
pulse_CH4_star = np.zeros(len(years))
pulse_N2O_star = np.zeros(len(years))
for i in range(len(years)):
    pulse_CH4_star[i] = a*pulse_CH4_100[i] - b*pulse_CH4_100[i-20]
    pulse_N2O_star[i] = a*pulse_N2O_100[i] - b*pulse_N2O_100[i-20]

# LWE
# Create Töplitz matrices (also appliable for sustained emissions later)
F_CO2 = EFmod(len(years),a_ghg[0,:])
F_CO2_inv = np.linalg.inv(F_CO2)
F_CH4 = EFmod(len(years),a_ghg[1,:])
F_N2O = EFmod(len(years),a_ghg[2,:])
# Calculate CO2-eq. emissions
pulse_CH4_lwe = F_CO2_inv@F_CH4@ np.array(pulse_CH4_fair['CH4'])*1e6/converter*1e-9
pulse_N2O_lwe = F_CO2_inv@F_N2O@ np.array(pulse_N2O_fair['N2O'])*1e6/converter*1e-9

# =============================================================================
# Sustained emissions
# =============================================================================
##### Unchanged emissions
# Methane only
sustained_CH4_fair = pd.DataFrame(0,index=range(len(years)),columns=[names])
sustained_CH4_fair.Year = years
sustained_CH4_fair.CH4 = 1

# Nitrous oxide only
sustained_N2O_fair = pd.DataFrame(0,index=range(len(years)),columns=[names])
sustained_N2O_fair.Year = years
sustained_N2O_fair.N2O = 1

##### Converted emissions
# GWP100
sustained_CH4_100 = np.array(sustained_CH4_fair)[:,3]*gwp100[2]*unit_ton[2]/converter*1e-9
sustained_N2O_100 = np.array(sustained_N2O_fair)[:,4]*gwp100[3]*unit_ton[3]/converter*1e-9

# GWP*
sustained_CH4_star = np.zeros(len(years))
sustained_N2O_star = np.zeros(len(years))
for i in range(20):
    sustained_CH4_star[i] = a*sustained_CH4_100[i]
    sustained_N2O_star[i] = a*sustained_N2O_100[i]
for i in range(20,len(years)):
    sustained_CH4_star[i] = a*sustained_CH4_100[i] - b*sustained_CH4_100[i-20]
    sustained_N2O_star[i] = a*sustained_N2O_100[i] - b*sustained_N2O_100[i-20]

# LWE
# Calculate CO2-eq. emissions
sustained_CH4_lwe = F_CO2_inv@F_CH4@ np.array(sustained_CH4_fair['CH4'])*1e6/converter*1e-9
sustained_N2O_lwe = F_CO2_inv@F_N2O@ np.array(sustained_N2O_fair['N2O'])*1e6/converter*1e-9

# =============================================================================
# Plots
# =============================================================================
##### CH4
fig, ax = plt.subplots(2, 2, figsize=(12,9), dpi=500)
ax[0,0].set_title('a) CO$_2$-eq. to pulse CH$_4$ emission')
ax[0,0].plot(years-2000, pulse_CH4_100*1e3*converter, label='GWP$_{100}$', color='tab:orange', ls='--')
ax[0,0].plot(years-2000, pulse_CH4_star*1e3*converter, label='GWP*', color='tab:green', ls='--')
ax[0,0].plot(years-2000, pulse_CH4_lwe*1e3*converter, label='LWE', color='tab:red', ls='--')
ax[0,1].set_title('b) CO$_2$-eq. to sustained CH$_4$ emission')
z1 = ax[0,1].plot(years-2000, sustained_CH4_100*1e3*converter, color='tab:orange', ls='--')
z2 = ax[0,1].plot(years-2000, sustained_CH4_star*1e3*converter, color='tab:green', ls='--')
z3 = ax[0,1].plot(years-2000, sustained_CH4_lwe*1e3*converter, color='tab:red', ls='--')
ax1 = ax[0,1].twinx()
z4 = ax1.plot(years-2000, T_only_CH4[:,0], color='k')
ax[0,0].set_xlabel('Years after pulse emission')
ax[0,0].set_ylabel('CO$_2$-eq. emissions (MtCO$_2$)')
ax[0,1].set_xlabel('Years after start of emission')
ax1.set_ylabel('Temperature response (°C)')
ax[0,0].legend()
ax1.legend(z1+z2+z3+z4,['GWP$_{100}$','GWP*','LWE','Warming'])

##### N2O
ax[1,0].set_title('c) CO$_2$-eq. to pulse N$_2$O emission')
ax[1,0].plot(years-2000, pulse_N2O_100*1e3*converter, label='GWP$_{100}$', color='tab:orange', ls='--')
ax[1,0].plot(years-2000, pulse_N2O_star*1e3*converter, label='GWP*', color='tab:green', ls='--')
ax[1,0].plot(years-2000, pulse_N2O_lwe*1e3*converter, label='LWE', color='tab:red', ls='--')
ax[1,1].set_title('d) CO$_2$-eq. to sustained N$_2$O emission')
z1 = ax[1,1].plot(years-2000, sustained_N2O_100*1e3*converter, color='tab:orange', ls='--')
z2 = ax[1,1].plot(years-2000, sustained_N2O_star*1e3*converter, color='tab:green', ls='--')
z3 = ax[1,1].plot(years-2000, sustained_N2O_lwe*1e3*converter, color='tab:red', ls='--')
ax1 = ax[1,1].twinx()
z4 = ax1.plot(years-2000, T_only_N2O[:,0], color='k')
ax[1,0].set_xlabel('Years after pulse emission')
ax[1,0].set_ylabel('CO$_2$-eq. emissions (MtCO$_2$)')
ax[1,1].set_xlabel('Years after start of emissions')
ax1.set_ylabel('Temperature response (°C)')
ax1.set_ylim([0,0.62])
ax[1,0].legend()
ax1.legend(z1+z2+z3+z4,['GWP$_{100}$','GWP*','LWE','Warming'])
plt.tight_layout()
plt.show()

# =============================================================================
# Change of coefficents (optimized for N2O) and case with sustained emissions
# =============================================================================
# Adapt parameters for such scenario
yr_num = 700
years = np.arange(2000,2000+yr_num)

F_volcanic = F_volcanic[:yr_num]
F_solar = F_solar[:yr_num]

# Temperature change for one gas
Tch4 = ETmod(yr_num,a_ghg[names.index('CH4')-2],a_prime)
Tn2o = ETmod(yr_num,a_ghg[names.index('N2O')-2],a_prime)
E1 = np.zeros([yr_num,2])
E1[0,0] = 1.
E1[:,1] = np.ones(yr_num)
T_only_CH4 = Tch4@E1
T_only_N2O = Tn2o@E1

##### Change of coefficients
a_bis = 1.21
b_bis = 1.05
t_bis = 50
##### Unchanged emissions
# Methane only
sustained_bis_CH4_fair = pd.DataFrame(0,index=range(len(years)),columns=[names])
sustained_bis_CH4_fair.Year = years
sustained_bis_CH4_fair.CH4 = 1

# Nitrous oxide only
sustained_bis_N2O_fair = pd.DataFrame(0,index=range(len(years)),columns=[names])
sustained_bis_N2O_fair.Year = years
sustained_bis_N2O_fair.N2O = 1

##### Converted emissions
# GWP100
sustained_bis_CH4_100 = np.array(sustained_bis_CH4_fair)[:,3]*gwp100[2]*unit_ton[2]/converter*1e-9
sustained_bis_N2O_100 = np.array(sustained_bis_N2O_fair)[:,4]*gwp100[3]*unit_ton[3]/converter*1e-9

# GWP*
sustained_bis_CH4_star = np.zeros(len(years))
sustained_bis_N2O_star = np.zeros(len(years))
for i in range(t_bis):
    sustained_bis_CH4_star[i] = a_bis*sustained_bis_CH4_100[i]
    sustained_bis_N2O_star[i] = a_bis*sustained_bis_N2O_100[i]
for i in range(t_bis,len(years)):
    sustained_bis_CH4_star[i] = a_bis*sustained_bis_CH4_100[i] - b_bis*sustained_bis_CH4_100[i-t_bis]
    sustained_bis_N2O_star[i] = a_bis*sustained_bis_N2O_100[i] - b_bis*sustained_bis_N2O_100[i-t_bis]

# LWE
# Create Töplitz matrices
F_CO2 = EFmod(len(years),a_ghg[0,:])
F_CO2_inv = np.linalg.inv(F_CO2)
F_CH4 = EFmod(len(years),a_ghg[1,:])
F_N2O = EFmod(len(years),a_ghg[2,:])

# Calculate CO2-eq. emissions
sustained_bis_CH4_lwe = F_CO2_inv@F_CH4@ np.array(sustained_bis_CH4_fair['CH4'])*1e6/converter*1e-9
sustained_bis_N2O_lwe = F_CO2_inv@F_N2O@ np.array(sustained_bis_N2O_fair['N2O'])*1e6/converter*1e-9

# Plot
##### CH4
fig, ax = plt.subplots(2, 1, figsize=(7,7), dpi=500)
ax[0].set_title(f'a) CO$_2$-eq. to sustained CH$_4$ emission\n a={a_bis}, b={b_bis} and t-{t_bis}')
z1 = ax[0].plot(years-2000, sustained_bis_CH4_100*1e3*converter, color='tab:orange', ls='--')
z2 = ax[0].plot(years-2000, sustained_bis_CH4_star*1e3*converter, color='tab:green', ls='--')
z3 = ax[0].plot(years-2000, sustained_bis_CH4_lwe*1e3*converter, color='tab:red', ls='--')
ax1 = ax[0].twinx()
z4 = ax1.plot(years-2000, T_only_CH4[:,0], color='k')
ax[0].set_xlabel('Years after start of emission')
ax[0].set_ylabel('CO$_2$-eq. emissions (MtCO$_2$)')
ax1.set_ylabel('Temperature response (°C)')
ax[0].legend()
ax1.legend(z1+z2+z3+z4,['GWP$_{100}$','GWP*','LWE','Warming'])

##### N2O
ax[1].set_title('b) CO$_2$-eq. to sustained N$_2$O emission')
z1 = ax[1].plot(years-2000, sustained_bis_N2O_100*1e3*converter, color='tab:orange', ls='--')
z2 = ax[1].plot(years-2000, sustained_bis_N2O_star*1e3*converter, color='tab:green', ls='--')
z3 = ax[1].plot(years-2000, sustained_bis_N2O_lwe*1e3*converter, color='tab:red', ls='--')
ax1 = ax[1].twinx()
z4 = ax1.plot(years-2000, T_only_N2O[:,0], color='k')
ax[1].set_xlabel('Years after start of emissions')
ax[1].set_ylabel('CO$_2$-eq. emissions (MtCO$_2$)')
ax1.set_ylabel('Temperature response (°C)')
ax1.set_ylim([0,0.16])
ax[1].legend()
ax1.legend(z1+z2+z3+z4,['GWP$_{100}$','GWP*','LWE','Warming'])
plt.tight_layout()
plt.show()







