#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  4 09:46:33 2023

@author: gabrieleoddo
"""

import numpy as np

# Climate forcers emissions considered + year
names = ['Year','FossilCO2','OtherCO2','CH4','N2O','SOx','CO','NMVOC','NOx','BC','OC','NH3','CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','CARB_TET','MCF','HCFC_22','HCFC_141B','HCFC_142B','HALON1211','HALON1202','HALON1301','HALON2402','CH3BR','CH3CL']

##### Set up model parameters, units of GtCO2
# AR5 official mass of atmosphere in kg
m_atm = 5.1352*1e18 
# molar mass of air
m_air = 28.97*1e-3  
# molar mass of each greenhouse gas
m_ghg = np.zeros(len(names)-2) 
# Initialize 10 parameters for each greenhouse gas
a_ghg = np.zeros((len(names)-2,10))

# =============================================================================
# Set parameters specific to each greenhouse gas 
# =============================================================================
"""
Parameters needed: Molar mass, atmmospheric lifetime(s), proportion of each lifetime
and radiative efficiency. Only CO2 has multiple lifetimes used, a single lifetime is
considered for each other greenhouse gas.
The lifetime and radiative efficiency are directly taken from AR6 IPCC
"""

##### CO2
# molar mass
m_ghg[0] = 44.01*1e-3
# % of each lifetime 
a_ghg[0,0:4] = [0.21787,0.22896,0.28454,0.26863]
# each possible lifetimes
a_ghg[0,4:8] = [1,381.330,34.7850,4.12370]
# convert Gt to ppm
a_ghg[0,8] = 1e12*1e6/m_ghg[0]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppm
a_ghg[0,9] = 1.33e-2 

##### CH4
# molar mass
m_ghg[1] = 16.04*1e-3 
# % of each lifetime
a_ghg[1,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[1,4:8] = [1,11.8,1,1]
 # convert Gt to ppb
a_ghg[1,8] = 1e12*1e9/m_ghg[1]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[1,9] = 5.99*1e-4 # 3.88*1e-4

##### N2O
# molar mass
m_ghg[2] = 44.01*1e-3 
# % of each lifetime
a_ghg[2,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[2,4:8] = [1,109,1,1]
 # convert Gt to ppb
a_ghg[2,8] = 1e12*1e9/m_ghg[2]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[2,9] = 3.2*1e-3

##### CF4
# molar mass
m_ghg[10] = 88.0*1e-3 
# % of each lifetime
a_ghg[10,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[10,4:8] = [1,50000,1,1]
 # convert Gt to ppb
a_ghg[10,8] = 1e12*1e9/m_ghg[10]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[10,9] = 0.099

##### C2F6
# molar mass
m_ghg[11] = 138.01*1e-3 
# % of each lifetime
a_ghg[11,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[11,4:8] = [1,10000,1,1]
 # convert Gt to ppb
a_ghg[11,8] = 1e12*1e9/m_ghg[11]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[11,9] = 0.261

##### C6F14
# molar mass of C6F14
m_ghg[12] = 338.04*1e-3 
# % of each lifetime
a_ghg[12,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[12,4:8] = [1,3100,1,1]
 # convert Gt to ppb
a_ghg[12,8] = 1e12*1e9/m_ghg[12]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[12,9] = 0.449

##### HFC23
# molar mass of HFC23
m_ghg[13] = 70.01*1e-3 
# % of each lifetime
a_ghg[13,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[13,4:8] = [1,228,1,1]
 # convert Gt to ppb
a_ghg[13,8] = 1e12*1e9/m_ghg[13]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[13,9] = 0.191

##### HFC32
# molar mass of HFC32
m_ghg[14] = 52.02*1e-3 
# % of each lifetime
a_ghg[14,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[14,4:8] = [1,5.4,1,1]
 # convert Gt to ppb
a_ghg[14,8] = 1e12*1e9/m_ghg[14]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[14,9] = 0.111

##### HFC43_10
# molar mass of HFC43_10
m_ghg[15] = 252.05*1e-3 
# % of each lifetime
a_ghg[15,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[15,4:8] = [1,17,1,1]
 # convert Gt to ppb
a_ghg[15,8] = 1e12*1e9/m_ghg[15]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[15,9] = 0.357

##### HFC125
# molar mass of HFC125 
m_ghg[16] = 120.02*1e-3 
# % of each lifetime
a_ghg[16,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[16,4:8] = [1,30,1,1]
 # convert Gt to ppb
a_ghg[16,8] = 1e12*1e9/m_ghg[16]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[16,9] = 0.234

##### HFC134a 
# molar mass of HFC134a
m_ghg[17] = 102.03*1e-3 
# % of each lifetime
a_ghg[17,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[17,4:8] = [1,14,1,1]
 # convert Gt to ppb
a_ghg[17,8] = 1e12*1e9/m_ghg[17]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[17,9] = 0.167

##### HFC143a
# molar mass of HFC143a
m_ghg[18] = 84.04*1e-3 
# % of each lifetime
a_ghg[18,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[18,4:8] = [1,51,1,1]
 # convert Gt to ppb
a_ghg[18,8] = 1e12*1e9/m_ghg[18]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[18,9] = 0.168

##### HFC227ea
# molar mass of HFC227ea
m_ghg[19] = 170.03*1e-3 
# % of each lifetime
a_ghg[19,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[19,4:8] = [1,36,1,1]
 # convert Gt to ppb
a_ghg[19,8] = 1e12*1e9/m_ghg[19]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[19,9] = 0.273

##### HFC245fa
# molar mass of HFC245fa
m_ghg[20] = 134.05*1e-3 
# % of each lifetime
a_ghg[20,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[20,4:8] = [1,7.9,1,1]
 # convert Gt to ppb
a_ghg[20,8] = 1e12*1e9/m_ghg[20]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[20,9] = 0.245

##### SF6
# molar mass of SF6
m_ghg[21] = 146.06*1e-3 
# % of each lifetime
a_ghg[21,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[21,4:8] = [1,3200,1,1]
 # convert Gt to ppb
a_ghg[21,8] = 1e12*1e9/m_ghg[21]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[21,9] = 0.567

##### CFC_11
# molar mass of CFC_11
m_ghg[22] = 137.37*1e-3 
# % of each lifetime
a_ghg[22,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[22,4:8] = [1,52,1,1]
 # convert Gt to ppb
a_ghg[22,8] = 1e12*1e9/m_ghg[22]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[22,9] = 0.259

##### CFC_12
# molar mass of CFC_12
m_ghg[23] = 120.91*1e-3 
# % of each lifetime
a_ghg[23,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[23,4:8] = [1,102,1,1]
 # convert Gt to ppb
a_ghg[23,8] = 1e12*1e9/m_ghg[23]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[23,9] = 0.32

##### CFC_113
# molar mass of CFC_113
m_ghg[24] = 187.375*1e-3 
# % of each lifetime
a_ghg[24,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[24,4:8] = [1,93,1,1]
 # convert Gt to ppb
a_ghg[24,8] = 1e12*1e9/m_ghg[24]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[24,9] = 0.301

##### CFC_114
# molar mass of CFC_114
m_ghg[25] = 170.92*1e-3 
# % of each lifetime
a_ghg[25,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[25,4:8] = [1,189,1,1]
 # convert Gt to ppb
a_ghg[25,8] = 1e12*1e9/m_ghg[25]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[25,9] = 0.314

##### CFC_115
# molar mass of CFC_115
m_ghg[26] = 154.466*1e-3 
# % of each lifetime
a_ghg[26,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[26,4:8] = [1,540,1,1]
 # convert Gt to ppb
a_ghg[26,8] = 1e12*1e9/m_ghg[26]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[26,9] = 0.246

##### CARB_TET
# molar mass of CARB_TET
m_ghg[27] = 153.83*1e-3 
# % of each lifetime
a_ghg[27,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[27,4:8] = [1,32,1,1]
 # convert Gt to ppb
a_ghg[27,8] = 1e12*1e9/m_ghg[27]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[27,9] = 0.166

##### MCF
# molar mass of MCF
m_ghg[28] = 133.4*1e-3 
# % of each lifetime
a_ghg[28,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[28,4:8] = [1,5,1,1]
 # convert Gt to ppb
a_ghg[28,8] = 1e12*1e9/m_ghg[28]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[28,9] = 0.065

##### HCFC_22
# molar mass of HCFC_22
m_ghg[29] = 86.47*1e-3 
# % of each lifetime
a_ghg[29,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[29,4:8] = [1,11.9,1,1]
 # convert Gt to ppb
a_ghg[29,8] = 1e12*1e9/m_ghg[29]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[29,9] = 0.214

##### HCFC_141B
# molar mass of HCFC_141B
m_ghg[30] = 116.95*1e-3 
# % of each lifetime
a_ghg[30,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[30,4:8] = [1,9.4,1,1]
 # convert Gt to ppb
a_ghg[30,8] = 1e12*1e9/m_ghg[30]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[30,9] = 0.161

##### HFCF_142B
# molar mass of HCFC_142B
m_ghg[31] = 100.495*1e-3 
# % of each lifetime
a_ghg[31,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[31,4:8] = [1,18,1,1]
 # convert Gt to ppb
a_ghg[31,8] = 1e12*1e9/m_ghg[31]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[31,9] = 0.193

##### HALON1211
# molar mass of HALON1211
m_ghg[32] = 165.36*1e-3 
# % of each lifetime
a_ghg[32,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[32,4:8] = [1,16,1,1]
 # convert Gt to ppb
a_ghg[32,8] = 1e12*1e9/m_ghg[32]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[32,9] = 0.3

##### HALON1202
# molar mass of HALON1202
m_ghg[33] = 209.82*1e-3 
# % of each lifetime
a_ghg[33,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[33,4:8] = [1,2.5,1,1]
 # convert Gt to ppb
a_ghg[33,8] = 1e12*1e9/m_ghg[33]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[33,9] = 0.272

##### HALON1301
# molar mass of HALON1301
m_ghg[34] = 148.91*1e-3 
# % of each lifetime
a_ghg[34,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[34,4:8] = [1,72,1,1]
 # convert Gt to ppb
a_ghg[34,8] = 1e12*1e9/m_ghg[34]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[34,9] = 0.299

##### HALON2402
# molar mass of HALON2402
m_ghg[35] = 259.823*1e-3 
# % of each lifetime
a_ghg[35,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[35,4:8] = [1,28,1,1]
 # convert Gt to ppb
a_ghg[35,8] = 1e12*1e9/m_ghg[35]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[35,9] = 0.312

##### CH3Br
# molar mass of CH3Br
m_ghg[36] = 94.94*1e-3 
# % of each lifetime
a_ghg[36,0:4] = [0,1.0,0,0]
# each possible lifetimes
a_ghg[36,4:8] = [1,0.8,1,1]
 # convert Gt to ppb
a_ghg[36,8] = 1e12*1e9/m_ghg[36]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[36,9] = 0.004

##### CH3Cl
# molar mass of CH3Cl
m_ghg[37] = 50.49*1e-3 
# % of each lifetime
a_ghg[37,0:4] = [0,0.9,0,0]
# each possible lifetimes
a_ghg[37,4:8] = [1,1,1,1]
 # convert Gt to ppb
a_ghg[37,8] = 1e12*1e9/m_ghg[37]/(m_atm/m_air)
# Radiative efficiency in W/m2/ppb
a_ghg[37,9] = 0.005










