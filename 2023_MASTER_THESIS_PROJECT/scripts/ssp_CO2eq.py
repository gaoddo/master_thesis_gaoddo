#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 17:15:33 2023

@author: gabrieleoddo
"""

# Import modules
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import fair
from fair.forward import fair_scm
from fair.SSPs import ssp119, ssp126, ssp245, ssp370, ssp434, ssp460, ssp534, ssp585

from fair.SSPs import ssp119, ssp126, ssp245, ssp370, ssp434, ssp460, ssp534, ssp585#, 
from fair.ancil import natural, cmip6_volcanic, cmip6_solar
from matplotlib import pyplot as plt

# Import functions
from thesis_functions import EFmod

#%%
###############################################################################
########################## Initial variables and data #########################
###############################################################################

path = '/Users/gabrieleoddo/Documents/ETHZ/Master_Thesis/FaIR_Gabriele/'

##### Lists of names with the climate forcers and pathways
# Different cases
case = ['True','GWP100','GWPstar','LWE']
# Climate forcers emissions considered + year
names = ['Year','FossilCO2','OtherCO2','CH4','N2O','SOx','CO','NMVOC','NOx','BC','OC','NH3','CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','CARB_TET','MCF','HCFC_22','HCFC_141B','HCFC_142B','HALON1211','HALON1202','HALON1301','HALON2402','CH3BR','CH3CL']
# Non-greenhouse gases
non_ghg_names = ['SOx','NMVOC','NOx','BC','OC','NH3', 'CO']
# Greenhouse gases (non-CO2)
other_ghg_names = sorted(list(set(names[3:]) - set(non_ghg_names)))
# Indirect greenhouse gases
indirect_ghg = ['BC','CO','NH3','NMVOC','NOx']
# Cooling impact climate forcers
neg_gwp_gases = ['OC','SOx']
# F-gases names
F_gases = ['CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','HCFC_22','HCFC_141B','HCFC_142B']
# Socioeconomic pathways considered
ssp = ['ssp119','ssp126','ssp245','ssp370','ssp434','ssp460','ssp534','ssp585']

##### Unit conversions
# Convert in t of emisssion
unit_ton = [1e9,1e9,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3]
# Convert from GtC to GtCO2
converter = 44.01/12.011 

##### Variables related to remaining global carbon budget
# Quantiles of probability (from AR6 IPCC)
quantiles = np.array([0.17, 0.33, 0.5, 0.66, 0.83])
# Zero emissions commitment (warming after net-zero emissions)
ZEC = 0
# Natural CH4 and N2O
naturalEmissions = natural.Emissions.emissions
# Zero emissions coefficients
zeroEmissions = np.array([0, 0, 0, 18.6992, 2.16883, 6.504991805, 0, 0, 0, 0, 0, 0, 0.0107, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 155.925, 3511.082])	
# Natural radiative forcing
F_solar = cmip6_solar.Forcing.solar
F_volcanic = cmip6_volcanic.Forcing.volcanic

##### Other global variables
# All considered years (from 1765 to 2500)
years = np.linspace(1765,2500,2500-1765+1)
# Length of the FaIR data
length = len(years)
# Pre-industrial period warming=0 corresponds to mean of T in 1850 ot 1900
mask1850_1900 = np.logical_and(years>=1850, years<=1900)
# First industrial year, reference for the temperature change
ref_year_index = np.where(years == 1900)[0][0]

##### Parameters for the different methods
# Coefficients of the GWP100
gwp100 = [1,1,27.9,273,-133.14,5.3,4.5,-11,900,-69,2.16,7380,12400,8620,14600,771,1600,3740,1530,5810,3600,962,25200,5560,11200,6520,9430,9600,2200,161,1960,860,2300,1930,216,7200,2170,2.43,5.54]
gwp100_ghg_only = [1,1,27.9,273,0,0,0,0,0,0,0,7380,12400,8620,14600,771,1600,3740,1530,5810,3600,962,25200,5560,11200,6520,9430,9600,2200,161,1960,860,2300,1930,216,7200,2170,2.43,5.54]

# Coefficients for the GWP* metric (optimized for CH4)
a = 4.53
b = 4.25
# Starting date for the conversion into CO2-equivalent emissions
start_date = np.where(years == 1900)[0][0] # year 1900

# Import parameters for the LWE method respective to each greenhouse gas
from lwe_parameters import a_ghg
# Initialize Töplitz matrices
F_ghg = np.zeros((len(names)-2,length,length))
# CO2 Töplitz matrix
F_ghg[0,:,:] = EFmod(length,a_ghg[0,:])
F_ref_inv = np.linalg.inv(F_ghg[0,:,:])

#%%
###############################################################################
################################ Load all SSPs ################################
###############################################################################
"""
Store emissions for all SSP scenario
Transform everything in CO2-eq emissions using the metrics/method and for 
 three cases for the non-greenhouse gases: not considered, considered without
any change, converted into CO2-equivalent emissions
Store Concentration, radiative Forcing, Temperature change and TCRE
associated to every scenario and every metric/method
"""
# CO2-equivalent emissions
CO2_emissions = np.zeros((length,len(ssp)))
eqCO2ssp_gwp100_ghg_only = np.zeros((length,len(ssp)))
eqCO2ssp_gwp_star_ghg_only = np.zeros((length,len(ssp)))
eqCO2ssp_lwe_ghg_only = np.zeros((length,len(ssp)))
eqCO2ssp_gwp100_all = np.zeros((length,len(ssp)))
eqCO2ssp_gwp_star_all = np.zeros((length,len(ssp)))

# Global air surface temperature change
T_fair = np.zeros((length-ref_year_index,len(ssp)))
T_100_ghg_only = np.zeros((length-ref_year_index,len(ssp)))
T_star_ghg_only = np.zeros((length-ref_year_index,len(ssp)))
T_lwe_ghg_only = np.zeros((length-ref_year_index,len(ssp)))
T_100_all = np.zeros((length-ref_year_index,len(ssp)))
T_star_all = np.zeros((length-ref_year_index,len(ssp)))

# Cumulative CO2 emissions
cumul_fair = np.zeros((length-ref_year_index,len(ssp)))
cumul_100 = np.zeros((length-ref_year_index,len(ssp)))
cumul_star = np.zeros((length-ref_year_index,len(ssp)))
cumul_lwe = np.zeros((length-ref_year_index,len(ssp)))

for fname in ssp:
    # Load SSP scenario
    emissions = pd.DataFrame(globals()['%s' % fname].Emissions.emissions, columns=names)
    CO2_emissions[:,list(ssp).index(fname)] = emissions.FossilCO2 + emissions.OtherCO2
    # Compute temperature change using FAIR (concentration and radiative forcing not useful)
    C_fair, F_fair, T = fair_scm(emissions=np.array(emissions), natural=naturalEmissions, F_volcanic=F_volcanic, F_solar=F_solar, useMultigas=True)
    T_fair[:,list(ssp).index(fname)] = T[ref_year_index:] - np.mean(T[mask1850_1900])

# =============================================================================
#   CO2-equivalent emissions using GWP100 metric
# =============================================================================
    ##### Non-greenhouse gases not considered at all
    # Creation of GWP100 for whole frame of emissions
    gwp100Emissions_ghg_only = pd.DataFrame(index=range(length),columns=names[1:])
    for i in range(length):
        gwp100Emissions_ghg_only.iloc[i] = emissions.iloc[i][1:]*gwp100_ghg_only*unit_ton/converter
    gwp100Emissions_ghg_only.insert(0,'Year',years)
    gwp100Emissions_ghg_only.FossilCO2 *= converter
    gwp100Emissions_ghg_only.OtherCO2 *= converter

    # Everything brought to CO2 column
    # Bringing to FossilCO2 or to OtherCO2 doens't change anything
    eqCO2_gwp100_ghg_only = pd.DataFrame(0,index=range(length),columns=names)
    eqCO2_gwp100_ghg_only.FossilCO2 = gwp100Emissions_ghg_only.sum(axis=1)
    eqCO2_gwp100_ghg_only.Year = years
    eqCO2_gwp100_ghg_only.FossilCO2 *= 1e-9
    # Equivalent emissions stored
    eqCO2ssp_gwp100_ghg_only[:,list(ssp).index(fname)] = eqCO2_gwp100_ghg_only.FossilCO2
        
    ##### Non-greenhouse gases transformed into CO2-equivalent emissions
    # Creation of GWP100 for whole frame of emissions
    gwp100Emissions_all = pd.DataFrame(index=range(length),columns=names[1:])
    for i in range(length):
        gwp100Emissions_all.iloc[i] = emissions.iloc[i][1:]*gwp100*unit_ton/converter
    gwp100Emissions_all.insert(0,'Year',years)
    gwp100Emissions_all.FossilCO2 *= converter
    gwp100Emissions_all.OtherCO2 *= converter

    # Everything brought to CO2 column
    eqCO2_gwp100_all = pd.DataFrame(0,index=range(length),columns=names)
    eqCO2_gwp100_all.FossilCO2 = gwp100Emissions_all.sum(axis=1)
    eqCO2_gwp100_all.Year = years
    eqCO2_gwp100_all.FossilCO2 = eqCO2_gwp100_all.FossilCO2*1e-9
    # Equivalent emissions stored
    eqCO2ssp_gwp100_all[:,list(ssp).index(fname)] = eqCO2_gwp100_all.FossilCO2
    
# =============================================================================
#   CO2-equivalent emissions using GWP* metric
# =============================================================================
    ##### Non-greenhouse gases not considered at all
    # Creation of GWP* for whole frame of emissions (based on GWP100 values)
    gwp_starEmissions_ghg_only = pd.DataFrame(index=range(length),columns=names[1:])
    gwp_starEmissions_ghg_only[:start_date] = gwp100Emissions_ghg_only.iloc[:start_date,1:]
    # Drop column with years for the calculation of GWP* emissions
    gwp100_temp = gwp100Emissions_ghg_only.iloc[:,1:]

    for i in range(start_date,length):
        gwp_starEmissions_ghg_only.loc[i, ~gwp_starEmissions_ghg_only.columns.isin(['FossilCO2', 'OtherCO2'])] = (a*gwp100_temp.loc[i, ~gwp_starEmissions_ghg_only.columns.isin(['FossilCO2', 'OtherCO2'])] - b*gwp100_temp.loc[i-20, ~gwp_starEmissions_ghg_only.columns.isin(['FossilCO2', 'OtherCO2'])])
    gwp_starEmissions_ghg_only.FossilCO2[start_date:] = gwp100Emissions_ghg_only.FossilCO2[start_date:]
    gwp_starEmissions_ghg_only.OtherCO2[start_date:] = gwp100Emissions_ghg_only.OtherCO2[start_date:]

    # Everything brought to CO2 column
    eqCO2_gwp_star_ghg_only = pd.DataFrame(0,index=range(length),columns=names)
    eqCO2_gwp_star_ghg_only.FossilCO2 = gwp_starEmissions_ghg_only.sum(axis=1)
    eqCO2_gwp_star_ghg_only.Year = years
    eqCO2_gwp_star_ghg_only.FossilCO2 *= 1e-9
    # Equivalent emissions stored
    eqCO2ssp_gwp_star_ghg_only[:,list(ssp).index(fname)] = eqCO2_gwp_star_ghg_only.FossilCO2
    
    ##### Non-greenhouse gases transformed into CO2-equivalent emissions
    # Creation of GWP* for whole frame of emissions (based on GWP100 values)
    gwp_starEmissions_all = pd.DataFrame(index=range(length),columns=names[1:])
    gwp_starEmissions_all[:start_date] = gwp100Emissions_all.iloc[:start_date,1:]
    # Drop column with years for the calculation of GWP* emissions
    gwp100_temp = gwp100Emissions_ghg_only.iloc[:,1:]

    for i in range(start_date,length):
        gwp_starEmissions_all.loc[i, ~gwp_starEmissions_all.columns.isin(['FossilCO2', 'OtherCO2'])] = (a*gwp100_temp.loc[i, ~gwp_starEmissions_all.columns.isin(['FossilCO2', 'OtherCO2'])] - b*gwp100_temp.loc[i-20, ~gwp_starEmissions_all.columns.isin(['FossilCO2', 'OtherCO2'])])
    gwp_starEmissions_all.FossilCO2[start_date:] = gwp100Emissions_all.FossilCO2[start_date:]
    gwp_starEmissions_all.OtherCO2[start_date:] = gwp100Emissions_all.OtherCO2[start_date:]

    # Everything brought to CO2 column
    eqCO2_gwp_star_all = pd.DataFrame(0,index=range(length),columns=names)
    eqCO2_gwp_star_all.FossilCO2 = gwp_starEmissions_all.sum(axis=1)
    eqCO2_gwp_star_all.Year = years
    eqCO2_gwp_star_all.FossilCO2 *= 1e-9
    # Equivalent emissions stored
    eqCO2ssp_gwp_star_all[:,list(ssp).index(fname)] = eqCO2_gwp_star_all.FossilCO2
    
# =============================================================================
#   CO2-equivalent emissions using LWE method
# =============================================================================
    ##### Non-greenhouse gases not considered at all
    # Calculate CO2-equivalent emissions for each greenhosue gas
    lweEmissions = pd.DataFrame(0,index=range(length),columns=names)
    lweEmissions.Year = years
    lweEmissions.FossilCO2 = np.array(emissions.FossilCO2)*1e9
    lweEmissions.OtherCO2 = np.array(emissions.OtherCO2)*1e9
    # Not possible for non-GHG
    for ghg in other_ghg_names[3:]:
        # Create a Töplitz matrix for each greenhouse gas
        F_ghg[names.index(ghg)-2,:,:] = EFmod(length,a_ghg[names.index(ghg)-2,:])
        # Calculate CO2-eq using LWE method
        lweEmissions[ghg] = F_ref_inv@F_ghg[names.index(ghg)-2,:,:]@np.array(emissions[ghg])*unit_ton[names.index(ghg)-1]/converter

    # Everything brought to CO2 column
    eqCO2_lwe_ghg_only = pd.DataFrame(0,index=range(length),columns=names)
    eqCO2_lwe_ghg_only.FossilCO2 = lweEmissions.sum(axis=1)
    eqCO2_lwe_ghg_only.Year = years
    eqCO2_lwe_ghg_only.FossilCO2 *= 1e-9
    # Equivalent emissions stored
    eqCO2ssp_lwe_ghg_only[:,list(ssp).index(fname)] = eqCO2_lwe_ghg_only.FossilCO2
    
# =============================================================================
# Cumulative CO2 of unchanged and converted emissions
# =============================================================================
# Unchanged emissions 
    cum_fair = np.array(emissions.FossilCO2[135:] + emissions.OtherCO2[135:])
    cum_fair *= converter # conversion from GtC to GtCO2
    cumul_fair[:,list(ssp).index(fname)] = cum_fair/converter
    
    # GWP100
    cumul_gwp100 = np.array(np.cumsum(eqCO2_gwp100_all.FossilCO2[135:]))
    cumul_gwp100 *= converter # conversion from GtC to GtCO2 
    cumul_100[:,list(ssp).index(fname)] = cumul_gwp100/converter

    # GWP*
    cumul_gwp_star = np.array(np.cumsum(eqCO2_gwp_star_all.FossilCO2[135:]))
    cumul_gwp_star *= converter # conversion from GtC to GtCO2
    cumul_star[:,list(ssp).index(fname)] = cumul_gwp_star/converter
    
    # LWE
    cum_lwe = np.array(np.cumsum(eqCO2_lwe_ghg_only.FossilCO2[135:]))
    cum_lwe *= converter # conversion from GtC to GtCO2
    cumul_lwe[:,list(ssp).index(fname)] = cum_lwe/converter

# =============================================================================
# Save SSP variables in csv files
# =============================================================================
# Temperature change 
pd.DataFrame(T_fair).to_csv(path+'from_rgcb_for_ssp/T_fair_ssp.csv')

# Cumulative emissions
pd.DataFrame(cumul_fair).to_csv(path+'from_rgcb_for_ssp/cumul_fair_ssp.csv')
pd.DataFrame(cumul_100).to_csv(path+'from_rgcb_for_ssp/cumul_100_ssp.csv')
pd.DataFrame(cumul_star).to_csv(path+'from_rgcb_for_ssp/cumul_star_ssp.csv')
pd.DataFrame(cumul_lwe).to_csv(path+'from_rgcb_for_ssp/cumul_lwe_ssp.csv')

# CO2 emissions
pd.DataFrame(CO2_emissions).to_csv(path+'from_rgcb_for_ssp/CO2_fair_ssp.csv')
pd.DataFrame(eqCO2ssp_gwp100_all).to_csv(path+'from_rgcb_for_ssp/CO2_100_ssp.csv')
pd.DataFrame(eqCO2ssp_gwp_star_all).to_csv(path+'from_rgcb_for_ssp/CO2_star_ssp.csv')
pd.DataFrame(eqCO2ssp_lwe_ghg_only).to_csv(path+'from_rgcb_for_ssp/CO2_lwe_ssp.csv')





