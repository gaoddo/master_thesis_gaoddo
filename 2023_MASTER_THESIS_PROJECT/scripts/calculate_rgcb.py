#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 12 11:18:54 2023

@author: gabrieleoddo
"""

# Import modules

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import fair
from fair.forward import fair_scm
from fair.SSPs import ssp119, ssp126, ssp245, ssp370, ssp434, ssp460, ssp534, ssp585
from fair.ancil import natural, cmip6_volcanic, cmip6_solar


# Import functions
from thesis_functions import compare_methods
from thesis_functions import EFmod
from thesis_functions import T_from_CO2eq
from thesis_functions import tcre_calculation
from thesis_functions import relative_error
# IPCC functions
from thesis_functions import ipcc_tcre_distribution
from thesis_functions import ipcc_RGCB
from thesis_functions import ipcc_EF


#%%
###############################################################################
########################## Initial variables and data #########################
###############################################################################

##### Directory
path = '/Users/gabrieleoddo/Documents/ETHZ/Master_Thesis/FaIR_Gabriele/'

##### Lists of names with the climate forcers and pathways
# Different cases
case = ['True','GWP100','GWPstar','LWE']
# Climate forcers emissions considered + year
names = ['Year','FossilCO2','OtherCO2','CH4','N2O','SOx','CO','NMVOC','NOx','BC','OC','NH3','CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','CARB_TET','MCF','HCFC_22','HCFC_141B','HCFC_142B','HALON1211','HALON1202','HALON1301','HALON2402','CH3BR','CH3CL']
# Non-greenhouse gases
non_ghg_names = ['SOx','NMVOC','NOx','BC','OC','NH3', 'CO']
# Greenhouse gases (non-CO2)
other_ghg_names = sorted(list(set(names[3:]) - set(non_ghg_names)))
# Indirect greenhouse gases
indirect_ghg = ['BC','CO','NH3','NMVOC','NOx']
# Cooling impact climate forcers
neg_gwp_gases = ['OC','SOx']
# F-gases names
F_gases = ['CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','HCFC_22','HCFC_141B','HCFC_142B']
# Socioeconomic pathways considered
ssp = ['ssp119','ssp126','ssp245','ssp370','ssp434','ssp460','ssp534','ssp585']

##### Unit conversions
# Convert in t of emisssion
unit_ton = [1e9,1e9,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3]
# Convert from GtC to GtCO2
converter = 44.01/12.011 

##### Data with climate forcers annual emissions
"""
Choose scenario (either from FaIR or manually from elsewhere)
From FaIR (SSP): ssp119, ssp126, ssp245, ssp370, ssp434, ssp460, ssp534, ssp585
Others: in input_scenarios
"""
emissions_name = 'ssp370'
if emissions_name == 'ssp119':
    emissions = pd.DataFrame(ssp119.Emissions.emissions, columns=names)
elif emissions_name == 'ssp126':
    emissions = pd.DataFrame(ssp126.Emissions.emissions, columns=names)
elif emissions_name == 'ssp245':
    emissions = pd.DataFrame(ssp245.Emissions.emissions, columns=names)
elif emissions_name == 'ssp370':
    emissions = pd.DataFrame(ssp370.Emissions.emissions, columns=names)
elif emissions_name == 'ssp434':
    emissions = pd.DataFrame(ssp434.Emissions.emissions, columns=names)
elif emissions_name == 'ssp460':
    emissions = pd.DataFrame(ssp460.Emissions.emissions, columns=names)
elif emissions_name == 'ssp534':
    emissions = pd.DataFrame(ssp534.Emissions.emissions, columns=names)
elif emissions_name == 'ssp585':
    emissions = pd.DataFrame(ssp585.Emissions.emissions, columns=names)
else:
    emissions = pd.read_csv(path+'input_scenarios/'+emissions_name+".csv")
    
##### Data from ssp_CO2eq.py
# Import temperature change and cumulative CO2 emissions from each SSP, for each method
T_ssp_fair = np.array(pd.read_csv(path+'from_rgcb_for_ssp/T_fair_ssp.csv'))[:,1:]
cumul_ssp_fair = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_fair_ssp.csv'))[:,1:]
cumul_ssp_100 = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_100_ssp.csv'))[:,1:]
cumul_ssp_star = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_star_ssp.csv'))[:,1:]
cumul_ssp_lwe = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_lwe_ssp.csv'))[:,1:]

CO2_ssp_fair = np.array(pd.read_csv(path+'from_rgcb_for_ssp/CO2_fair_ssp.csv'))[:,1:]
CO2_ssp_100 = np.array(pd.read_csv(path+'from_rgcb_for_ssp/CO2_100_ssp.csv'))[:,1:]
CO2_ssp_star = np.array(pd.read_csv(path+'from_rgcb_for_ssp/CO2_star_ssp.csv'))[:,1:]
CO2_ssp_lwe = np.array(pd.read_csv(path+'from_rgcb_for_ssp/CO2_lwe_ssp.csv'))[:,1:]

##### Variables related to remaining global carbon budget
# Quantiles of probability
quantiles = np.array([0.17, 0.33, 0.5, 0.66, 0.83])
# Zero emissions commitment (warming after net-zero emissions)
ZEC = 0
# Temperature targets for the remaining global carbon budget
T_target = np.array([1.5,1.8,2.0])
# Earth climate feedback (from AR6 IPCC) in GtCO2/°C
EF_avg = 7.1*converter
EF_std = 26.7*converter
# Natural CH4 and N2O
naturalEmissions = natural.Emissions.emissions
# Zero emissions coefficients
zeroEmissions = np.array([0, 0, 0, 18.6992, 2.16883, 6.504991805, 0, 0, 0, 0, 0, 0, 0.0107, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 155.925, 3511.082])	
# Natural radiative forcing
F_solar = cmip6_solar.Forcing.solar
F_volcanic = cmip6_volcanic.Forcing.volcanic

##### Other global variables
# All considered years (from 1765 to 2500)
years = np.array(emissions.Year).astype('int')
# Length of the FaIR data
length = len(years)
# Pre-industrial period warming=0 corresponds to mean of T in 1850 ot 1900
mask1850_1900 = np.logical_and(years>=1850, years<=1900)
# First industrial year, reference for the temperature change
ref_year_index = np.where(years == 1900)[0][0]
# Starting date for the special case
start_sim = np.where(years == 2015)[0][0]
# Number of elements in the TCRE/EF distribution
n_return = 10000

###### Parameters for the different methods
# Coefficients of the GWP100
gwp100 = [1,1,27.9,273,-133.14,5.3,4.5,-11,900,-69,2.16,7380,12400,8620,14600,771,1600,3740,1530,5810,3600,962,25200,5560,11200,6520,9430,9600,2200,161,1960,860,2300,1930,216,7200,2170,2.43,5.54]
gwp100_ghg_only = [1,1,27.9,273,0,0,0,0,0,0,0,7380,12400,8620,14600,771,1600,3740,1530,5810,3600,962,25200,5560,11200,6520,9430,9600,2200,161,1960,860,2300,1930,216,7200,2170,2.43,5.54]

# Coefficients for the GWP* metric (optimized for CH4)
a = 4.53
b = 4.25
# Import parameters for the LWE method respective to each greenhouse gas
from lwe_parameters import a_ghg
# Initialize Töplitz matrices
F_ghg = np.zeros((len(names)-2,length,length))
# CO2 Töplitz matrix
F_ghg[0,:,:] = EFmod(length,a_ghg[0,:])
F_ref_inv = np.linalg.inv(F_ghg[0,:,:])


#%%
###############################################################################
############################### Data formating ###############################
###############################################################################

# Load scenario database
# From https://doi.org/10.5281/zenodo.5886911
ipcc_scenarios = pd.read_csv(path+'IPCC_data&code/AR6_Scenarios_Database_World/AR6_Scenarios_Database_World_v1.csv')
ipcc_scenarios = ipcc_scenarios[ipcc_scenarios.Variable.str.contains('Emissions')].reset_index(drop=True)
# Merge Model and Scenario to have distinct scenarios
ipcc_scenarios['Scenario_distinct'] = ipcc_scenarios[['Model', 'Scenario']].agg('/'.join, axis=1)
ipcc_scenarios.insert(0,'Model_scenario',ipcc_scenarios.Scenario_distinct)
ipcc_scenarios = ipcc_scenarios.drop(columns=['Model','Scenario','Scenario_distinct'])
# Delete scenarios only containing 1 line
ghg_per_scenario = ipcc_scenarios.groupby('Model_scenario').Model_scenario.count()
scenarios_to_delete = ghg_per_scenario.index[ghg_per_scenario < 19]
ipcc_scenarios = ipcc_scenarios.drop(np.where(np.isin(ipcc_scenarios.Model_scenario,scenarios_to_delete))[0]).reset_index(drop=True)
# From the rest of the scenarios, 2 have only CO2 emissions. They won't be considered
ipcc_scenarios = ipcc_scenarios[ipcc_scenarios.Model_scenario != 'IEA Energy Technology Perspectives Model 2020/B2DS'].reset_index(drop=True)
ipcc_scenarios = ipcc_scenarios[ipcc_scenarios.Model_scenario != 'LUT-ESTM 2.0/BPS'].reset_index(drop=True)
ghg_per_scenario = ipcc_scenarios.groupby('Model_scenario').Model_scenario.count()

years_ipcc_scenarios = np.linspace(1995,2100,2100-1995+1).astype('int').astype('str')
years_ipcc_and_before = np.linspace(1765,2100,2100-1765+1).astype('int')
ipcc_mask1850_1900 = np.logical_and(years_ipcc_and_before>=1850, years_ipcc_and_before<=1900)
first_year = np.where(years_ipcc_scenarios == '2015')[0][0]

# Adapt naturalEmissions, F_solar and F_volcanic in correspondance to ipcc years
naturalEmissions_ipcc = naturalEmissions[:len(years_ipcc_and_before)]
F_volcanic_ipcc = F_volcanic[:len(years_ipcc_and_before)]
F_solar_ipcc = F_solar[:len(years_ipcc_and_before)]

# Creation of historical F-Gases emissions (1765-2015) in CO2-equivalent and using GWP100
CO2eq_ipcc = np.zeros(len(years_ipcc_and_before))
for gas in F_gases:
    CO2eq_ipcc[:250] += emissions[gas][:250]*gwp100[names.index(gas)-1]/1e6


#%%
###############################################################################
##################### Creation of GHG emissions DataFrame #####################
###############################################################################
# Initialize arrays of temperature and cumulative CO2/CO2-equivalent emissions averaged over each scenario
T_all_ipcc_fair = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))

# For simulations with all years
cumul_all_ipcc_fair = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
cumul_all_ipcc_100 = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
cumul_all_ipcc_star = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
cumul_all_ipcc_lwe = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))

# Sensitivity tests: if True, two special cases disregarding an initial condition
# are done in addition. One case disregards historical emissions, another disregards
# every climate forcer different than CO2, CH4 or N2O

special_cases = True

if special_cases:
    # For simulations with only future years
    cumul_all_ipcc_fair_from15 = np.zeros((len(years_ipcc_and_before[start_sim:]),len(ipcc_scenarios.Model_scenario.unique())))
    cumul_all_ipcc_100_from15 = np.zeros((len(years_ipcc_and_before[start_sim:]),len(ipcc_scenarios.Model_scenario.unique())))
    cumul_all_ipcc_star_from15 = np.zeros((len(years_ipcc_and_before[start_sim:]),len(ipcc_scenarios.Model_scenario.unique())))
    cumul_all_ipcc_lwe_from15 = np.zeros((len(years_ipcc_and_before[start_sim:]),len(ipcc_scenarios.Model_scenario.unique())))
    T_all_ipcc_fair_from15 = np.zeros((len(years_ipcc_and_before[start_sim:]),len(ipcc_scenarios.Model_scenario.unique())))

    # For simulations with only CO2, CH4 and N2O
    cumul_all_ipcc_fair_simp = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
    cumul_all_ipcc_100_simp = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
    cumul_all_ipcc_star_simp = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
    cumul_all_ipcc_lwe_simp = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
    T_all_ipcc_fair_simp = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))


for scenario in ipcc_scenarios.Model_scenario.unique():
# =============================================================================
#   True emissions, no metric used
# =============================================================================
    # Building a dataframe compatible with FAIR
    ipcc_emissions = pd.DataFrame(0,index=range(len(years_ipcc_and_before)),columns=names)
    ipcc_emissions.Year = years_ipcc_and_before
    
    if 'AR6 climate diagnostics|Infilled|Emissions|CO2' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        CO2_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'AR6 climate diagnostics|Infilled|Emissions|CO2', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
    else:
        CO2_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|CO2', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
    ipcc_emissions.loc[:249,('FossilCO2')] = emissions.FossilCO2[:250]*converter
    ipcc_emissions.loc[:249,('OtherCO2')] = emissions.OtherCO2[:250]*converter
    ipcc_emissions.loc[250:,('FossilCO2')] = CO2_ipcc[first_year:]
    
    if 'AR6 climate diagnostics|Infilled|Emissions|CH4' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        CH4_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'AR6 climate diagnostics|Infilled|Emissions|CH4', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
    else:
        CH4_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|CH4', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
    ipcc_emissions.loc[:250,('CH4')] = emissions.CH4[:250]/1e3
    ipcc_emissions.loc[250:,('CH4')] = CH4_ipcc[first_year:]
    
    if 'AR6 climate diagnostics|Infilled|Emissions|N2O' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        N2O_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'AR6 climate diagnostics|Infilled|Emissions|N2O', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e6
    else:
        N2O_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|N2O', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e6
    ipcc_emissions.loc[:250,('N2O')] = emissions.N2O[:250]/1e3
    ipcc_emissions.loc[250:,('N2O')] = N2O_ipcc[first_year:]
    
    if 'AR6 climate diagnostics|Infilled|Emissions|F-gases' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        Fgas_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'AR6 climate diagnostics|Infilled|Emissions|F-gases', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        CO2eq_ipcc[250:] = Fgas_ipcc[first_year:]
        Fgas_ipcc_100 = CO2eq_ipcc.copy()
    elif 'Emissions|F-gases' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        Fgas_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|F-gases', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        CO2eq_ipcc[250:] = Fgas_ipcc[first_year:]
        Fgas_ipcc_100 = CO2eq_ipcc.copy()
    else:    
        Fgas_ipcc_100 = np.zeros(len(years_ipcc_and_before))
    
    if 'Emissions|CO' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        CO_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|CO', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        ipcc_emissions.loc[:250,('CO')] = emissions.CO[:250]/1e3
        ipcc_emissions.loc[250:,('CO')] = CO_ipcc[first_year:]
    else:
        CO_ipcc = np.zeros(len(years_ipcc_and_before))
    
    if 'Emissions|BC' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        BC_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|BC', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        ipcc_emissions.loc[:250,('BC')] = emissions.BC[:250]/1e3
        ipcc_emissions.loc[250:,('BC')] = BC_ipcc[first_year:]
    else:
        BC_ipcc = np.zeros(len(years_ipcc_and_before))
    
    if 'Emissions|NH3' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        NH3_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|NH3', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        ipcc_emissions.loc[:250,('NH3')] = emissions.NH3[:250]/1e3
        ipcc_emissions.loc[250:,('NH3')] = NH3_ipcc[first_year:]
    else:
        NH3_ipcc = np.zeros(len(years_ipcc_and_before))
        
    if 'Emissions|NOx' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        NOx_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|NOx', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        ipcc_emissions.loc[:250,('NOx')] = emissions.NOx[:250]/1e3
        ipcc_emissions.loc[250:,('NOx')] = NOx_ipcc[first_year:]
    else:
        NOx_ipcc = np.zeros(len(years_ipcc_and_before))

    if 'Emissions|OC' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        OC_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|OC', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        ipcc_emissions.loc[:250,('OC')] = emissions.OC[:250]/1e3
        ipcc_emissions.loc[250:,('OC')] = OC_ipcc[first_year:]
    else:
        OC_ipcc = np.zeros(len(years_ipcc_and_before))

    if 'Emissions|Sulfur' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        SOx_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|Sulfur', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        ipcc_emissions.loc[:250,('SOx')] = emissions.SOx[:250]/1e3
        ipcc_emissions.loc[250:,('SOx')] = SOx_ipcc[first_year:]
    else:
        SOx_ipcc = np.zeros(len(years_ipcc_and_before))

    if 'Emissions|VOC' in ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].Variable.unique():
        NMVOC_ipcc = np.array(ipcc_scenarios[ipcc_scenarios.Model_scenario == scenario].loc[ipcc_scenarios.Variable == 'Emissions|VOC', years_ipcc_scenarios]).reshape(len(years_ipcc_scenarios),)/1e3
        ipcc_emissions.loc[:250,('NMVOC')] = emissions.NMVOC[:250]/1e3
        ipcc_emissions.loc[250:,('NMVOC')] = NMVOC_ipcc[first_year:]
    else:
        NMVOC_ipcc = np.zeros(len(years_ipcc_and_before))
    
    if special_cases:
        # Do a copy of emissions for later tests
        ipcc_emissions_simplified = ipcc_emissions.copy()
        
# =============================================================================
#   Metric-used emissions (all CO2-eq. emissions)
# =============================================================================
    ##### GWP100
    ipcc_gwp100 = np.zeros((len(ipcc_emissions),len(names)-1))
    ipcc_gwp100 = np.array(ipcc_emissions.loc[:,ipcc_emissions.columns != 'Year'])*gwp100/converter
    
    if special_cases:
        # Do a copy of emissions for later tests
        ipcc_gwp100_from15 = ipcc_gwp100.copy()
        Fgas_ipcc_100_from15 = Fgas_ipcc_100.copy()

    ##### GWP*
    ipcc_gwp_star = np.zeros((len(ipcc_emissions),len(names)-1))
    ipcc_gwp_star[:,0] = np.array(ipcc_emissions.FossilCO2)/converter
    ipcc_gwp_star[:,1] = np.array(ipcc_emissions.OtherCO2)/converter
    Fgas_ipcc_star = np.zeros(len(ipcc_emissions))
    for i in range(ref_year_index,len(ipcc_emissions)):
        # All gases
        ipcc_gwp_star[i,2:] = a*ipcc_gwp100[i,2:] - b*ipcc_gwp100[i-20,2:]
        # F-gases
        Fgas_ipcc_star[i] = a*Fgas_ipcc_100[i] - b*Fgas_ipcc_100[i-20]
    
    if special_cases:
        # Do a copy of emissions for later tests
        ipcc_gwp_star_from15 = ipcc_gwp_star[start_sim:].copy()
        Fgas_ipcc_star_from15 = Fgas_ipcc_star[start_sim:].copy()
    
    # Bring together GWP emissions to have CO2-equivalent emissions
    ipcc_gwp100 = np.nansum(ipcc_gwp100, axis=1) + Fgas_ipcc_100
    ipcc_gwp_star = np.nansum(ipcc_gwp_star, axis=1) + Fgas_ipcc_star
    
    # Adapt ipcc_emissions for true warming in FAIR
    for name in names[1:]:
        ipcc_emissions.loc[:,(name)] *= 1e9/unit_ton[names.index(name)-1]
    ipcc_emissions.loc[:,('FossilCO2')] /= converter
    ipcc_emissions.loc[:,('OtherCO2')] /= converter

    # Avoid NAN problems
    ipcc_emissions[np.isnan(ipcc_emissions)] = 0

    ##### LWE
    ipcc_lwe = np.zeros((len(ipcc_emissions),len(names)-1))
    ipcc_lwe[:,0] = np.array(ipcc_emissions.FossilCO2)*1e9
    ipcc_lwe[:,1] = np.array(ipcc_emissions.OtherCO2)*1e9
    F_ghg_ipcc = np.zeros((len(names)-2,len(ipcc_emissions),len(ipcc_emissions)))
    F_ghg_ipcc[0,:,:] = EFmod(len(ipcc_emissions),a_ghg[0,:])
    F_ref_inv_ipcc = np.linalg.inv(F_ghg_ipcc[0,:,:])
    # Not possible for non-GHG
    for ghg in other_ghg_names[3:]:
        # Create a Töplitz matrix for each greenhouse gas
        F_ghg_ipcc[names.index(ghg)-2,:,:] = EFmod(len(ipcc_emissions),a_ghg[names.index(ghg)-2,:])
        # Calculate CO2-eq. emissions using LWE method
        ipcc_lwe[:,names.index(ghg)-1] = F_ref_inv_ipcc@F_ghg_ipcc[names.index(ghg)-2,:,:]@np.array(ipcc_emissions[ghg])*1e6/converter
    
    if special_cases:
        # For sensitivity test: only CO2, CH4 and N2O considered
        ipcc_lwe_simp = np.nansum(ipcc_lwe[:,:4], axis=1)*1e-9
    
    # Bring together LWE emissions to have CO2-equivalent emissions
    ipcc_lwe = np.nansum(ipcc_lwe, axis=1)*1e-9

    # Use of FaIR to calculate concentration, forcing and temperature change
    _, _, T_ipcc_fair = fair_scm(emissions=np.array(ipcc_emissions), natural=naturalEmissions_ipcc, F_volcanic=F_volcanic_ipcc, F_solar=F_solar_ipcc, useMultigas=True)
    # Set T_ipcc_fair to start at industrial period
    T_ipcc_fair = T_ipcc_fair[ref_year_index:] - np.mean(T_ipcc_fair[ipcc_mask1850_1900])
    
    # Calculation of the cumulative emissions
    cumul_emissions_ipcc_fair = np.array(np.nancumsum(ipcc_emissions.FossilCO2[ref_year_index:] + ipcc_emissions.OtherCO2[ref_year_index:]))
    cumul_emissions_ipcc_100 = np.array(np.nancumsum(ipcc_gwp100[ref_year_index:]))
    cumul_emissions_ipcc_star = np.array(np.nancumsum(ipcc_gwp_star[ref_year_index:]))
    cumul_emissions_ipcc_lwe = np.array(np.nancumsum(ipcc_lwe[ref_year_index:]))
    
    cumul_all_ipcc_fair[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_emissions_ipcc_fair
    cumul_all_ipcc_100[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_emissions_ipcc_100
    cumul_all_ipcc_star[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_emissions_ipcc_star
    cumul_all_ipcc_lwe[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_emissions_ipcc_lwe
   
    # Average T_ipcc for all 1683 scenarios and different metrics
    T_all_ipcc_fair[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = T_ipcc_fair
    
# =============================================================================
#   Special cases: sensitivity tests with simplified conditions
# =============================================================================
    if special_cases:
        ##### Start the simulation in 2015
        
        # GWP100 do not need to be changed
        
        # Application of GWP* (only the first 20 years)
        for i in range(20):
            ipcc_gwp_star_from15[i,2:] = a*ipcc_gwp100_from15[start_sim+i,2:]
            Fgas_ipcc_star_from15[i] = a*Fgas_ipcc_100_from15[start_sim+i]
            
        # Application of LWE method
        ipcc_lwe_from15 = np.zeros((len(ipcc_emissions[start_sim:]),len(names)-1))
        ipcc_lwe_from15[:,0] = np.array(ipcc_emissions.FossilCO2[start_sim:])*1e9
        ipcc_lwe_from15[:,1] = np.array(ipcc_emissions.OtherCO2[start_sim:])*1e9
        F_from15_ipcc = np.zeros((len(names)-2,len(ipcc_emissions[start_sim:]),len(ipcc_emissions[start_sim:])))
        F_from15_ipcc[0,:,:] = EFmod(len(ipcc_emissions[start_sim:]),a_ghg[0,:])
        F_ref_inv_from15_ipcc = np.linalg.inv(F_from15_ipcc[0,:,:])
        # Not possible for non-GHG
        for ghg in other_ghg_names[3:]:
            # Create a Töplitz matrix for each greenhouse gas
            F_from15_ipcc[names.index(ghg)-2,:,:] = EFmod(len(ipcc_emissions[start_sim:]),a_ghg[names.index(ghg)-2,:])
            # Calculate CO2-eq using LWE method
            ipcc_lwe_from15[:,names.index(ghg)-1] = F_ref_inv_from15_ipcc@F_from15_ipcc[names.index(ghg)-2,:,:]@np.array(ipcc_emissions[ghg][start_sim:])*1e6/converter
        
        # Bring together greenhouse gases to have CO2-equivalent emissions
        ipcc_gwp_star_from15 = np.nansum(ipcc_gwp_star_from15, axis=1) + Fgas_ipcc_star_from15
        ipcc_lwe_from15 = np.nansum(ipcc_lwe_from15, axis=1)*1e-9
        
        # Run FAIR disregarding historical emissions
        _, _, T_fair_from15 = fair_scm(emissions=np.array(ipcc_emissions)[start_sim:], natural=naturalEmissions_ipcc[start_sim:], F_volcanic=F_volcanic_ipcc[start_sim:], F_solar=F_solar_ipcc[start_sim:], useMultigas=True)
        
        # Special cumulative CO2 emissions
        cumul_ipcc_fair_from15 = np.array(np.nancumsum(ipcc_emissions.FossilCO2[start_sim:] + ipcc_emissions.OtherCO2[start_sim:]))
        cumul_ipcc_100_from15 = np.array(np.nancumsum(ipcc_gwp100[start_sim:]))
        cumul_ipcc_star_from15 = np.array(np.nancumsum(ipcc_gwp_star_from15))
        cumul_ipcc_lwe_from15 = np.array(np.nancumsum(ipcc_lwe_from15))
        
        cumul_all_ipcc_fair_from15[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_fair_from15
        cumul_all_ipcc_100_from15[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_100_from15
        cumul_all_ipcc_star_from15[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_star_from15
        cumul_all_ipcc_lwe_from15[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_lwe_from15
        
        T_all_ipcc_fair_from15[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = T_fair_from15
        
        ##### Consider only CO2, CH4 and N2O
        # Build a dataframe compatible with FAIR
        ipcc_emissions_simp = pd.DataFrame(0,index=range(len(years_ipcc_and_before)),columns=names)
        ipcc_emissions_simp.Year = years_ipcc_and_before
        
        # Copy emissions from CO2, CH4 and N2O
        ipcc_emissions_simp.FossilCO2 = ipcc_emissions_simplified.FossilCO2
        ipcc_emissions_simp.OtherCO2 = ipcc_emissions_simplified.OtherCO2
        ipcc_emissions_simp.CH4 = ipcc_emissions_simplified.CH4
        ipcc_emissions_simp.N2O = ipcc_emissions_simplified.N2O
        
        # Application of GWP100
        ipcc_gwp100_simp = np.zeros((len(ipcc_emissions_simp),len(names)-1))
        ipcc_gwp100_simp = np.array(ipcc_emissions_simp.loc[:,ipcc_emissions_simp.columns != 'Year'])*gwp100/converter

        # Application of GWP*
        ipcc_gwp_star_simp = np.zeros((len(ipcc_emissions_simp),len(names)-1))
        ipcc_gwp_star_simp[:,0] = np.array(ipcc_emissions_simp.FossilCO2)/converter
        ipcc_gwp_star_simp[:,1] = np.array(ipcc_emissions_simp.OtherCO2)/converter
        for i in range(ref_year_index,len(ipcc_emissions_simp)):
            # CH4 and N2O
            ipcc_gwp_star_simp[i,2:] = a*ipcc_gwp100_simp[i,2:] - b*ipcc_gwp100_simp[i-20,2:]
        
        # Application of LWE done at the same time than normal case

        # Bring together greenhouse gases to have CO2-equivalent emissions
        ipcc_gwp100_simp = np.nansum(ipcc_gwp100_simp, axis=1)
        ipcc_gwp_star_simp = np.nansum(ipcc_gwp_star_simp, axis=1)
        
        # Adapt ipcc_emissions_simp for true warming in FAIR
        for name in names[1:]:
            ipcc_emissions_simp.loc[:,(name)] *= 1e9/unit_ton[names.index(name)-1]
        ipcc_emissions_simp.loc[:,('FossilCO2')] /= converter
        ipcc_emissions_simp.loc[:,('OtherCO2')] /= converter
        # Avoid NAN problems
        ipcc_emissions_simp[np.isnan(ipcc_emissions_simp)] = 0
        
        # Run FAIR with only CO2, CH4 and N2O
        _, _, T_fair_simp = fair_scm(emissions=np.array(ipcc_emissions_simp), natural=naturalEmissions_ipcc, F_volcanic=F_volcanic_ipcc, F_solar=F_solar_ipcc, useMultigas=True)
        # Set T_ipcc_fair to start at industrial period
        T_fair_simp = T_fair_simp[ref_year_index:] - np.mean(T_fair_simp[ipcc_mask1850_1900])

        # Special cumulative CO2 emissions
        cumul_ipcc_fair_simp = np.array(np.nancumsum(ipcc_emissions.FossilCO2[ref_year_index:] + ipcc_emissions.OtherCO2[ref_year_index:]))
        cumul_ipcc_100_simp = np.array(np.nancumsum(ipcc_gwp100_simp[ref_year_index:]))
        cumul_ipcc_star_simp = np.array(np.nancumsum(ipcc_gwp_star_simp[ref_year_index:]))
        cumul_ipcc_lwe_simp = np.array(np.nancumsum(ipcc_lwe_simp[ref_year_index:]))
        
        cumul_all_ipcc_fair_simp[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_fair_simp
        cumul_all_ipcc_100_simp[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_100_simp
        cumul_all_ipcc_star_simp[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_star_simp
        cumul_all_ipcc_lwe_simp[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = cumul_ipcc_lwe_simp
        
        T_all_ipcc_fair_simp[:,list(ipcc_scenarios.Model_scenario.unique()).index(scenario)] = T_fair_simp

# =============================================================================
# Cumulative CO2 of SSP scenarios
# =============================================================================
cumul_ssp_fair = np.zeros((length-ref_year_index,len(ssp)))
cumul_ssp_100 = np.zeros((length-ref_year_index,len(ssp)))
cumul_ssp_star = np.zeros((length-ref_year_index,len(ssp)))
cumul_ssp_lwe = np.zeros((length-ref_year_index,len(ssp)))
for i in range(len(ssp)):
    # Unchanged emissions 
    cum_fair = np.array(np.cumsum(CO2_ssp_fair[135:,i]))
    cumul_ssp_fair[:,i] = cum_fair
    
    # GWP100
    cum_100 = np.array(np.cumsum(CO2_ssp_100[135:,i]))
    cumul_ssp_100[:,i] = cum_100

    # GWP*
    cum_star = np.array(np.cumsum(CO2_ssp_star[135:,i]))
    cumul_ssp_star[:,i] = cum_star
    
    # LWE
    cum_lwe = np.array(np.cumsum(CO2_ssp_lwe[135:,i]))
    cumul_ssp_lwe[:,i] = cum_lwe

# =============================================================================
# Store cumulative emissions and warming from sensitivity tests
# =============================================================================
if special_cases:  
    ##### Simulation starting in 2015
    # Temperature change 
    pd.DataFrame(T_all_ipcc_fair_from15).to_csv(path+'from_rgcb_for_ssp/T_all_ipcc_fair_from15.csv')
    
    # Cumulative emissions
    pd.DataFrame(cumul_all_ipcc_fair_from15).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_fair_from15.csv')
    pd.DataFrame(cumul_all_ipcc_100_from15).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_100_from15.csv')
    pd.DataFrame(cumul_all_ipcc_star_from15).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_star_from15.csv')
    pd.DataFrame(cumul_all_ipcc_lwe_from15).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_lwe_from15.csv')
    
    ##### Simulation only with CO2, CH4 and N2O
    # Temperature change 
    pd.DataFrame(T_all_ipcc_fair_simp).to_csv(path+'from_rgcb_for_ssp/T_all_ipcc_fair_simp.csv')
    
    # Cumulative emissions
    pd.DataFrame(cumul_all_ipcc_fair_simp).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_fair_simp.csv')
    pd.DataFrame(cumul_all_ipcc_100_simp).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_100_simp.csv')
    pd.DataFrame(cumul_all_ipcc_star_simp).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_star_simp.csv')
    pd.DataFrame(cumul_all_ipcc_lwe_simp).to_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_lwe_simp.csv')


#%%
###############################################################################
#################### TCRE calculation using every scenario ####################
############################ TCRE- not considered #############################
###############################################################################

# =============================================================================
# TCRE calculation
# =============================================================================
# Reshape temperature change and cumulative emissions
##### IPCC scenarios
T_1d_ipcc_fair = T_all_ipcc_fair.reshape(len(T_all_ipcc_fair)*len(T_all_ipcc_fair[0]))
cumul_1d_ipcc_fair = cumul_all_ipcc_fair.reshape(len(cumul_all_ipcc_fair)*len(cumul_all_ipcc_fair[0]))
cumul_1d_ipcc_100 = cumul_all_ipcc_100.reshape(len(cumul_all_ipcc_fair)*len(cumul_all_ipcc_fair[0]))
cumul_1d_ipcc_star = cumul_all_ipcc_star.reshape(len(cumul_all_ipcc_fair)*len(cumul_all_ipcc_fair[0]))
cumul_1d_ipcc_lwe = cumul_all_ipcc_lwe.reshape(len(cumul_all_ipcc_fair)*len(cumul_all_ipcc_fair[0]))
# Put together cumulative emissions and temperature change
TC_ipcc_fair = np.vstack((cumul_1d_ipcc_fair,T_1d_ipcc_fair)).T
TC_ipcc_100 = np.vstack((cumul_1d_ipcc_100,T_1d_ipcc_fair)).T
TC_ipcc_star = np.vstack((cumul_1d_ipcc_star,T_1d_ipcc_fair)).T
TC_ipcc_lwe = np.vstack((cumul_1d_ipcc_lwe,T_1d_ipcc_fair)).T

##### SSP scenarios
T_1d_ssp_fair = np.delete(T_ssp_fair,[3,5,7],axis=1).reshape(len(T_ssp_fair)*(len(ssp)-3))
cumul_1d_ssp_fair = np.delete(cumul_ssp_fair,[3,5,7],axis=1).reshape(len(cumul_ssp_fair)*(len(ssp)-3))
cumul_1d_ssp_100 = np.delete(cumul_ssp_100,[3,5,7],axis=1).reshape(len(cumul_ssp_fair)*(len(ssp)-3))
cumul_1d_ssp_star = np.delete(cumul_ssp_star,[3,5,7],axis=1).reshape(len(cumul_ssp_fair)*(len(ssp)-3))
cumul_1d_ssp_lwe = np.delete(cumul_ssp_lwe,[3,5,7],axis=1).reshape(len(cumul_ssp_fair)*(len(ssp)-3))
# Put together cumulative emissions and temperature change
TC_ssp_fair = np.vstack((cumul_1d_ssp_fair,T_1d_ssp_fair)).T
TC_ssp_100 = np.vstack((cumul_1d_ssp_100,T_1d_ssp_fair)).T
TC_ssp_star = np.vstack((cumul_1d_ssp_star,T_1d_ssp_fair)).T
TC_ssp_lwe = np.vstack((cumul_1d_ssp_lwe,T_1d_ssp_fair)).T

# Bring IPCC and SSP scenarios together
TC_fair = np.vstack((TC_ipcc_fair,TC_ssp_fair))
TC_100 = np.vstack((TC_ipcc_100,TC_ssp_100))
TC_star = np.vstack((TC_ipcc_star,TC_ssp_star))
TC_lwe = np.vstack((TC_ipcc_lwe,TC_ssp_lwe))
# Delete outliers (negative values in temperature change column)
TC_fair = np.delete(TC_fair, np.where(TC_fair[:,1] < 0)[0], 0)
TC_100 = np.delete(TC_100, np.where(TC_100[:,1] < 0)[0], 0)
TC_star = np.delete(TC_star, np.where(TC_star[:,1] < 0)[0], 0)
TC_lwe = np.delete(TC_lwe, np.where(TC_lwe[:,1] < 0)[0], 0)

##### TCRE calculation for unchanged and converted pemisisons
# Low and high TCRE boundaries (in °C per GtCO2)
extrema = np.array([0.15,0.95])

tcre_quantile_fair = tcre_calculation(TC_fair, extrema)
tcre_quantile_100 = tcre_calculation(TC_100, extrema)
tcre_quantile_star = tcre_calculation(TC_star, extrema)
tcre_quantile_lwe = tcre_calculation(TC_lwe, extrema)

# =============================================================================
# Plot the different TCREs
# =============================================================================
##### Plot TCRE of each case
fig, ax = plt.subplots(2, 2, figsize=(12,9), dpi=500)
# fig.suptitle('TCRE range of CO$_2$/CO$_2$-eq. emissions')
# Unchanged emissions
ax[0,0].scatter(TC_fair[:,0]*converter, TC_fair[:,1], alpha = 0.02, s=2, label='All scenarios', color='tab:blue')
ax[0,0].plot(np.linspace(0,np.max(TC_fair[:,0]*converter)),np.linspace(0,np.max(TC_fair[:,0]*converter))*tcre_quantile_fair['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_fair['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_fair[:,0]*converter))*tcre_quantile_fair['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_fair[:,0]*converter))*tcre_quantile_fair['0.95'].loc[0][0]/1e3
ax[0,0].fill_between(np.linspace(0,np.max(TC_fair[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_fair['0.05'].loc[0][0]:.3f}-{tcre_quantile_fair['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[0,0].set_title('a) Unchanged emissions (°C/TtCO$_2$)')
ax[0,0].set_xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
ax[0,0].set_ylabel('GSAT change relative to 1900 (°C)')
ax[0,0].legend(loc='upper left')

# GWP100
ax[0,1].scatter(TC_100[:,0]*converter, TC_100[:,1], alpha = 0.02, s=2, label='All scenarios', color='tab:blue')
ax[0,1].plot(np.linspace(0,np.max(TC_100[:,0]*converter)),np.linspace(0,np.max(TC_100[:,0]*converter))*tcre_quantile_100['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_100['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_100[:,0]*converter))*tcre_quantile_100['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_100[:,0]*converter))*tcre_quantile_100['0.95'].loc[0][0]/1e3
ax[0,1].fill_between(np.linspace(0,np.max(TC_100[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_100['0.05'].loc[0][0]:.3f}-{tcre_quantile_100['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[0,1].set_title('b) GWP$_{100}$-converted emissions (°C/TtCO$_2$)')
ax[0,1].set_xlabel('Cumulative CO$_2$-eq. emissions (GtCO$_2$)')
ax[0,1].set_ylabel('GSAT change relative to 1900 (°C)')
ax[0,1].legend(loc='upper left')

# GWP*
ax[1,0].scatter(TC_star[:,0]*converter, TC_star[:,1], alpha = 0.02, s=2, label='All scenarios', color='tab:blue')
ax[1,0].plot(np.linspace(0,np.max(TC_star[:,0]*converter)),np.linspace(0,np.max(TC_star[:,0]*converter))*tcre_quantile_star['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_star['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_star[:,0]*converter))*tcre_quantile_star['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_star[:,0]*converter))*tcre_quantile_star['0.95'].loc[0][0]/1e3
ax[1,0].fill_between(np.linspace(0,np.max(TC_star[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_star['0.05'].loc[0][0]:.3f}-{tcre_quantile_star['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[1,0].set_title('c) GWP*-converted emissions (°C/TtCO$_2$)')
ax[1,0].set_xlabel('Cumulative CO$_2$-eq. emissions (GtCO$_2$)')
ax[1,0].set_ylabel('GSAT change relative to 1900 (°C)')
ax[1,0].legend(loc='upper left')

# LWE
ax[1,1].scatter(TC_lwe[:,0]*converter, TC_lwe[:,1], alpha = 0.02, s=2, label='All scenarios', color='tab:blue')
ax[1,1].plot(np.linspace(0,np.max(TC_lwe[:,0]*converter)),np.linspace(0,np.max(TC_lwe[:,0]*converter))*tcre_quantile_lwe['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_lwe['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_lwe[:,0]*converter))*tcre_quantile_lwe['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_lwe[:,0]*converter))*tcre_quantile_lwe['0.95'].loc[0][0]/1e3
ax[1,1].fill_between(np.linspace(0,np.max(TC_lwe[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_lwe['0.05'].loc[0][0]:.3f}-{tcre_quantile_lwe['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[1,1].set_title('d) LWE-converted emissions (°C/TtCO$_2$)')
ax[1,1].set_xlabel('Cumulative CO$_2$-eq. emissions (GtCO$_2$)')
ax[1,1].set_ylabel('GSAT change relative to 1900 (°C)')
ax[1,1].legend(loc='upper left')
plt.tight_layout()
plt.show()

##### Plot TCRE distribution using IPCC functions
# Probability that results fit between the low and high value of TCRE
likelihood = 0.6827
# All possible distributions
tcre_dist = ['normal','lognormal','lognormal mean match']

# TCRE distribution for unchanged emissions
low = tcre_quantile_fair['0.05'].loc[0][0]
high = tcre_quantile_fair['0.95'].loc[0][0]
tcre_nor_fair = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])
tcre_log_fair = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[1])
tcre_lmm_fair = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[2])

# TCRE distribution for GWP100
low = tcre_quantile_100['0.05'].loc[0][0]
high = tcre_quantile_100['0.95'].loc[0][0]
tcre_nor_100 = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])
tcre_log_100 = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[1])
tcre_lmm_100 = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[2])

# TCRE distribution for GWP*
low = tcre_quantile_star['0.05'].loc[0][0]
high = tcre_quantile_star['0.95'].loc[0][0]
tcre_nor_star = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])
tcre_log_star = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[1])
tcre_lmm_star = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[2])

# TCRE distribution for LWE
low = tcre_quantile_lwe['0.05'].loc[0][0]
high = tcre_quantile_lwe['0.95'].loc[0][0]
tcre_nor_lwe = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])
tcre_log_lwe = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[1])
tcre_lmm_lwe = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[2])


#%%
###############################################################################
#################### TCRE calculation using every scenario ####################
############################## TCRE- considered ###############################
###############################################################################

# =============================================================================
#  Separate cumulative net positive, resp. negative, CO2 emissions
# =============================================================================
# Cumulative emissions and temperature change are separated similarily
# The separation is done based on the unchanged emissions
cumul_net_neg_fair = np.array([])
cumul_net_pos_fair = np.array([])
cumul_net_neg_100 = np.array([])
cumul_net_pos_100 = np.array([])
cumul_net_neg_star = np.array([])
cumul_net_pos_star = np.array([])
cumul_net_neg_lwe = np.array([])
cumul_net_pos_lwe = np.array([])

T_net_neg_fair = np.array([])
T_net_pos_fair = np.array([])

# =============================================================================
# IPCC scenarios
# =============================================================================
for i in range(len(cumul_all_ipcc_fair[0])):
    ##### For unchanged emissions
    # Cumulative emissions
    neg_index = np.where(np.diff(cumul_all_ipcc_fair[:,i]) < 0)[0]
    pos_index = np.where(np.diff(cumul_all_ipcc_fair[:,i]) >= 0)[0]
    neg_cumul = cumul_all_ipcc_fair[neg_index,i]
    pos_cumul = cumul_all_ipcc_fair[pos_index,i]
    cumul_net_neg_fair = np.hstack((cumul_net_neg_fair,neg_cumul))
    cumul_net_pos_fair = np.hstack((cumul_net_pos_fair,pos_cumul))
    # Temperature change
    neg_T = T_all_ipcc_fair[neg_index,i]
    pos_T = T_all_ipcc_fair[pos_index,i]
    T_net_neg_fair = np.hstack((T_net_neg_fair,neg_T))
    T_net_pos_fair = np.hstack((T_net_pos_fair,pos_T))
    
    ##### For GWP100
    neg_cumul = cumul_all_ipcc_100[neg_index,i]
    pos_cumul = cumul_all_ipcc_100[pos_index,i]
    cumul_net_neg_100 = np.hstack((cumul_net_neg_100,neg_cumul))
    cumul_net_pos_100 = np.hstack((cumul_net_pos_100,pos_cumul))
    
    ##### For GWP*
    neg_cumul = cumul_all_ipcc_star[neg_index,i]
    pos_cumul = cumul_all_ipcc_star[pos_index,i]
    cumul_net_neg_star = np.hstack((cumul_net_neg_star,neg_cumul))
    cumul_net_pos_star = np.hstack((cumul_net_pos_star,pos_cumul))
    
    ##### For LWE
    neg_cumul = cumul_all_ipcc_lwe[neg_index,i]
    pos_cumul = cumul_all_ipcc_lwe[pos_index,i]
    cumul_net_neg_lwe = np.hstack((cumul_net_neg_lwe,neg_cumul))
    cumul_net_pos_lwe = np.hstack((cumul_net_pos_lwe,pos_cumul))
    
# =============================================================================
# SSP scenarios
# =============================================================================
for i in range(len(cumul_ssp_fair[0])):
    ##### For unchanged emissions
    # Cumulative emissions
    neg_index = np.where(np.diff(cumul_ssp_fair[:,i]) < 0)[0]
    pos_index = np.where(np.diff(cumul_ssp_fair[:,i]) >= 0)[0]
    neg_cumul = cumul_ssp_fair[neg_index,i]
    pos_cumul = cumul_ssp_fair[pos_index,i]
    cumul_net_neg_fair = np.hstack((cumul_net_neg_fair,neg_cumul))
    cumul_net_pos_fair = np.hstack((cumul_net_pos_fair,pos_cumul))
    # Temperature change
    neg_T = T_ssp_fair[neg_index,i]
    pos_T = T_ssp_fair[pos_index,i]
    T_net_neg_fair = np.hstack((T_net_neg_fair,neg_T))
    T_net_pos_fair = np.hstack((T_net_pos_fair,pos_T))
    
    ##### For GWP100
    neg_cumul = cumul_ssp_100[neg_index,i]
    pos_cumul = cumul_ssp_100[pos_index,i]
    cumul_net_neg_100 = np.hstack((cumul_net_neg_100,neg_cumul))
    cumul_net_pos_100 = np.hstack((cumul_net_pos_100,pos_cumul))

    ##### For GWP*
    neg_cumul = cumul_ssp_star[neg_index,i]
    pos_cumul = cumul_ssp_star[pos_index,i]
    cumul_net_neg_star = np.hstack((cumul_net_neg_star,neg_cumul))
    cumul_net_pos_star = np.hstack((cumul_net_pos_star,pos_cumul))
    
    ##### For LWE
    neg_cumul = cumul_ssp_lwe[neg_index,i]
    pos_cumul = cumul_ssp_lwe[pos_index,i]
    cumul_net_neg_lwe = np.hstack((cumul_net_neg_lwe,neg_cumul))
    cumul_net_pos_lwe = np.hstack((cumul_net_pos_lwe,pos_cumul))

# =============================================================================
# Put together cumulative emissions and temperature change
# =============================================================================
TC_net_neg_fair = np.vstack((cumul_net_neg_fair,T_net_neg_fair)).T
TC_net_neg_100 = np.vstack((cumul_net_neg_100,T_net_neg_fair)).T
TC_net_neg_star = np.vstack((cumul_net_neg_star,T_net_neg_fair)).T
TC_net_neg_lwe = np.vstack((cumul_net_neg_lwe,T_net_neg_fair)).T

TC_net_pos_fair = np.vstack((cumul_net_pos_fair,T_net_pos_fair)).T
TC_net_pos_100 = np.vstack((cumul_net_pos_100,T_net_pos_fair)).T
TC_net_pos_star = np.vstack((cumul_net_pos_star,T_net_pos_fair)).T
TC_net_pos_lwe = np.vstack((cumul_net_pos_lwe,T_net_pos_fair)).T
# Delete outliers of net_pos (negative values in temperature change column)
TC_net_pos_fair = np.delete(TC_net_pos_fair, np.where(TC_net_pos_fair[:,1] < 0)[0], 0)
TC_net_pos_100 = np.delete(TC_net_pos_100, np.where(TC_net_pos_100[:,1] < 0)[0], 0)
TC_net_pos_star = np.delete(TC_net_pos_star, np.where(TC_net_pos_star[:,1] < 0)[0], 0)
TC_net_pos_lwe = np.delete(TC_net_pos_lwe, np.where(TC_net_pos_lwe[:,1] < 0)[0], 0)

# =============================================================================
# TCRE calculation
# =============================================================================
##### TCRE of cumulative net positive CO2 emissions
# Low and high TCRE boundaries (in °C per GtCO2)
extrema = np.array([0.15,0.95])

tcre_pos_quantile_fair = tcre_calculation(TC_net_pos_fair, extrema)
tcre_pos_quantile_100 = tcre_calculation(TC_net_pos_100, extrema)
tcre_pos_quantile_star = tcre_calculation(TC_net_pos_star, extrema)
tcre_pos_quantile_lwe = tcre_calculation(TC_net_pos_lwe, extrema)

##### TCRE of cumulative net negative CO2 emissions
tcre_neg_quantile_fair = tcre_calculation(TC_net_neg_fair, extrema)
tcre_neg_quantile_100 = tcre_calculation(TC_net_neg_100, extrema)
tcre_neg_quantile_star = tcre_calculation(TC_net_neg_star, extrema)
tcre_neg_quantile_lwe = tcre_calculation(TC_net_neg_lwe, extrema)

# =============================================================================
# Plot the different TCRE-s
# =============================================================================
##### Plot TCRE of each case
fig, ax = plt.subplots(2, 2, figsize=(12,9), dpi=500)
# fig.suptitle('TCRE- range of CO$_2$/CO$_2$-eq. emissions')
# Unchanged emissions
ax[0,0].scatter(TC_net_neg_fair[:,0]*converter, TC_net_neg_fair[:,1], alpha = 0.05, s=2, label='All scenarios', color='tab:blue')
ax[0,0].plot(np.linspace(0,np.max(TC_net_neg_fair[:,0]*converter)),np.linspace(0,np.max(TC_net_neg_fair[:,0]*converter))*tcre_neg_quantile_fair['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_neg_quantile_fair['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_net_neg_fair[:,0]*converter))*tcre_neg_quantile_fair['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_net_neg_fair[:,0]*converter))*tcre_neg_quantile_fair['0.95'].loc[0][0]/1e3
ax[0,0].fill_between(np.linspace(0,np.max(TC_net_neg_fair[:,0]*converter)),high,low, label=f"5-95% range of {tcre_neg_quantile_fair['0.05'].loc[0][0]:.3f}-{tcre_neg_quantile_fair['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[0,0].set_title('a) Unchanged emissions (°C/TtCO$_2$)')
ax[0,0].set_xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
ax[0,0].set_ylabel('GSAT change relative to 1900 (°C)')
ax[0,0].set_xlim([np.min(TC_net_neg_fair[:,0]*converter),np.max(TC_net_neg_fair[:,0]*converter)])
ax[0,0].set_ylim([0.8,2.7])
ax[0,0].legend(loc='upper left')

# GWP100
ax[0,1].scatter(TC_net_neg_100[:,0]*converter, TC_net_neg_100[:,1], alpha = 0.02, s=2, label='All scenarios', color='tab:blue')
ax[0,1].plot(np.linspace(0,np.max(TC_net_neg_100[:,0]*converter)),np.linspace(0,np.max(TC_net_neg_100[:,0]*converter))*tcre_neg_quantile_100['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_neg_quantile_100['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_net_neg_100[:,0]*converter))*tcre_neg_quantile_100['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_net_neg_100[:,0]*converter))*tcre_neg_quantile_100['0.95'].loc[0][0]/1e3
ax[0,1].fill_between(np.linspace(0,np.max(TC_net_neg_100[:,0]*converter)),high,low, label=f"5-95% range of {tcre_neg_quantile_100['0.05'].loc[0][0]:.3f}-{tcre_neg_quantile_100['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[0,1].set_title('b) GWP$_{100}$-converted emissions (°C/TtCO$_2$)')
ax[0,1].set_xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
ax[0,1].set_ylabel('GSAT change relative to 1900 (°C)')
ax[0,1].set_xlim([np.min(TC_net_neg_100[:,0]*converter),np.max(TC_net_neg_100[:,0]*converter)])
ax[0,1].set_ylim([0.8,2.7])
ax[0,1].legend(loc='upper left')

# GWP*
ax[1,0].scatter(TC_net_neg_star[:,0]*converter, TC_net_neg_star[:,1], alpha = 0.02, s=2, label='All scenarios', color='tab:blue')
ax[1,0].plot(np.linspace(0,np.max(TC_net_neg_star[:,0]*converter)),np.linspace(0,np.max(TC_net_neg_star[:,0]*converter))*tcre_neg_quantile_star['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_neg_quantile_star['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_net_neg_star[:,0]*converter))*tcre_neg_quantile_star['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_net_neg_star[:,0]*converter))*tcre_neg_quantile_star['0.95'].loc[0][0]/1e3
ax[1,0].fill_between(np.linspace(0,np.max(TC_net_neg_star[:,0]*converter)),high,low, label=f"5-95% range of {tcre_neg_quantile_star['0.05'].loc[0][0]:.3f}-{tcre_neg_quantile_star['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[1,0].set_title('c) GWP*-converted emissions (°C/TtCO$_2$)')
ax[1,0].set_xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
ax[1,0].set_ylabel('GSAT change relative to 1900 (°C)')
ax[1,0].set_xlim([np.min(TC_net_neg_star[:,0]*converter),np.max(TC_net_neg_star[:,0]*converter)])
ax[1,0].set_ylim([0.8,2.7])
ax[1,0].legend(loc='upper left')

# LWE
ax[1,1].scatter(TC_net_neg_lwe[:,0]*converter, TC_net_neg_lwe[:,1], alpha = 0.02, s=2, label='All scenarios', color='tab:blue')
ax[1,1].plot(np.linspace(0,np.max(TC_net_neg_lwe[:,0]*converter)),np.linspace(0,np.max(TC_net_neg_lwe[:,0]*converter))*tcre_neg_quantile_lwe['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_neg_quantile_lwe['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_net_neg_lwe[:,0]*converter))*tcre_neg_quantile_lwe['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_net_neg_lwe[:,0]*converter))*tcre_neg_quantile_lwe['0.95'].loc[0][0]/1e3
ax[1,1].fill_between(np.linspace(0,np.max(TC_net_neg_lwe[:,0]*converter)),high,low, label=f"5-95% range of {tcre_neg_quantile_lwe['0.05'].loc[0][0]:.3f}-{tcre_neg_quantile_lwe['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
ax[1,1].set_title('d) LWE-converted emissions (°C/TtCO$_2$)')
ax[1,1].set_xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
ax[1,1].set_ylabel('GSAT change relative to 1900 (°C)')
ax[1,1].set_xlim([np.min(TC_net_neg_lwe[:,0]*converter),np.max(TC_net_neg_lwe[:,0]*converter)])
ax[1,1].set_ylim([0.8,2.7])
ax[1,1].legend(loc='upper left')
plt.tight_layout()
plt.show()


#%%
###############################################################################
################## Calculate warming with converted emissions #################
###############################################################################

# =============================================================================
# TCRE- not considered)
# =============================================================================
# Parameters for TCRE- specifically set
net_neg_emi = False
shift = len(T_ipcc_fair)-1

# Initialize temperature change for IPCC and SSP scenarios
T_ipcc_100 = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
T_ipcc_star = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
T_ipcc_lwe = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))

T_ssp_100 = np.zeros((len(years[ref_year_index:]),len(ssp)))
T_ssp_star = np.zeros((len(years[ref_year_index:]),len(ssp)))
T_ssp_lwe = np.zeros((len(years[ref_year_index:]),len(ssp)))

# Initialize fictive temperature change for IPCC and SSP scenarios
T_ipcc_fict = np.zeros((len(years_ipcc_and_before[ref_year_index:]),len(ipcc_scenarios.Model_scenario.unique())))
T_ssp_fict = np.zeros((len(years[ref_year_index:]),len(ssp)))

# For IPCC scenarios
for i in range(len(cumul_all_ipcc_fair[0])):
    T_ipcc_100[:,i] = T_from_CO2eq(np.array([tcre_quantile_100['0.5'].loc[0][0],0])/1e3*converter, cumul_all_ipcc_100[:,i], len(T_ipcc_fair), net_neg_emi, shift)
    T_ipcc_star[:,i] = T_from_CO2eq(np.array([tcre_quantile_star['0.5'].loc[0][0],0])/1e3*converter, cumul_all_ipcc_star[:,i], len(T_ipcc_fair), net_neg_emi, shift)
    T_ipcc_lwe[:,i] = T_from_CO2eq(np.array([tcre_quantile_lwe['0.5'].loc[0][0],0])/1e3*converter, cumul_all_ipcc_lwe[:,i], len(T_ipcc_fair), net_neg_emi, shift)
    T_ipcc_fict[:,i] = T_from_CO2eq(np.array([tcre_quantile_fair['0.5'].loc[0][0],0])/1e3*converter, cumul_all_ipcc_fair[:,i], len(T_ipcc_fair), net_neg_emi, shift)

# For SSP scenarios
for i in range(len(cumul_ssp_fair[0])):
    T_ssp_100[:,i] = T_from_CO2eq(np.array([tcre_quantile_100['0.5'].loc[0][0],0])/1e3*converter, cumul_ssp_100[:,i], len(T_ipcc_fair), net_neg_emi, shift)
    T_ssp_star[:,i] = T_from_CO2eq(np.array([tcre_quantile_star['0.5'].loc[0][0],0])/1e3*converter, cumul_ssp_star[:,i], len(T_ipcc_fair), net_neg_emi, shift)
    T_ssp_lwe[:,i] = T_from_CO2eq(np.array([tcre_quantile_lwe['0.5'].loc[0][0],0])/1e3*converter, cumul_ssp_lwe[:,i], len(T_ipcc_fair), net_neg_emi, shift)
    T_ssp_fict[:,i] = T_from_CO2eq(np.array([tcre_quantile_fair['0.5'].loc[0][0],0])/1e3*converter, cumul_ssp_fair[:,i], len(T_ipcc_fair), net_neg_emi, shift)

# =============================================================================
# Average over each scenario
# =============================================================================
##### Maximum and minimum scenario, and average (for plots)
# Maximum temperature scenario
T_max_ipcc_fair = np.amax(T_all_ipcc_fair, axis=1)
T_max_ipcc_100 = np.amax(T_ipcc_100, axis=1)
T_max_ipcc_star = np.amax(T_ipcc_star, axis=1)
T_max_ipcc_lwe = np.amax(T_ipcc_lwe, axis=1)
# Minimum temperature scenario 
T_min_ipcc_fair = np.amin(T_all_ipcc_fair, axis=1)
T_min_ipcc_100 = np.amin(T_ipcc_100, axis=1)
T_min_ipcc_star = np.amin(T_ipcc_star, axis=1)
T_min_ipcc_lwe = np.amin(T_ipcc_lwe, axis=1)

# Temperature change averaged over each scenario
T_average_ipcc_fair = np.nanmean(T_all_ipcc_fair, axis=1)
T_average_ipcc_100 = np.nanmean(T_ipcc_100, axis=1)
T_average_ipcc_star = np.nanmean(T_ipcc_star, axis=1)
T_average_ipcc_lwe = np.nanmean(T_ipcc_lwe, axis=1)
T_average_ipcc_fict = np.nanmean(T_ipcc_fict, axis=1)

# Cumulative CO2 and CO2-eq. emissions (average over each scenario)
cumul_average_ipcc_fair = np.median(cumul_all_ipcc_fair, axis=1)
cumul_average_ipcc_100 = np.median(cumul_all_ipcc_100, axis=1)
cumul_average_ipcc_star = np.median(cumul_all_ipcc_star, axis=1)
cumul_average_ipcc_lwe = np.median(cumul_all_ipcc_lwe, axis=1)

# Store in cvs files for later tests
pd.DataFrame(cumul_average_ipcc_fair).to_csv(path+'from_rgcb_for_ssp/cumul_avg_fair.csv')
pd.DataFrame(cumul_average_ipcc_100).to_csv(path+'from_rgcb_for_ssp/cumul_avg_100.csv')
pd.DataFrame(cumul_average_ipcc_star).to_csv(path+'from_rgcb_for_ssp/cumul_avg_star.csv')
pd.DataFrame(cumul_average_ipcc_lwe).to_csv(path+'from_rgcb_for_ssp/cumul_avg_lwe.csv')

# Plot actual warming from all climatge forcers with FAIR and expected warming 
# from TCRE and cumulative emissions
compare_methods(cumul_average_ipcc_fair, cumul_average_ipcc_100, cumul_average_ipcc_star, cumul_average_ipcc_lwe,
                T_average_ipcc_fair, T_average_ipcc_100, T_average_ipcc_star, T_average_ipcc_lwe, T_average_ipcc_fict,
                years_ipcc_and_before[ref_year_index:], top_xlim=2100)


#%%
###############################################################################
################### Error of methods relative to FAIR model ###################
###############################################################################

# =============================================================================
# Temperature error (from each scenario)
# =============================================================================
##### Warming from FAIR vs warming from effective TCRE
# Relative error from each scenario and year
err_ipcc_eff = relative_error(T_all_ipcc_fair[115:,:], T_ipcc_fict[115:,:])
err_ssp_eff = relative_error(T_ssp_fair[115:,:], T_ssp_fict[115:,:])
err_ssp_eff = np.delete(err_ssp_eff,[3,5,7],axis=1)
err_eff = np.hstack((err_ipcc_eff.reshape(len(err_ipcc_eff)*len(err_ipcc_eff[0])),err_ssp_eff.reshape(len(err_ssp_eff)*len(err_ssp_eff[0]))))
# Mean and standard deviation of the error
mean_err_eff = np.mean(err_eff)
std_err_eff = np.std(err_eff)

##### Warming from FAIR vs warming from GWP100
# Relative error from each scenario and year
err_ipcc_100 = relative_error(T_all_ipcc_fair[115:,:], T_ipcc_100[115:,:])
err_ssp_100 = relative_error(T_ssp_fair[115:,:], T_ssp_100[115:,:])
err_ssp_100 = np.delete(err_ssp_100,[3,5,7],axis=1)
err_100 = np.hstack((err_ipcc_100.reshape(len(err_ipcc_100)*len(err_ipcc_100[0])),err_ssp_100.reshape(len(err_ssp_100)*len(err_ssp_100[0]))))
# Mean and standard deviation of the error
mean_err_100 = np.mean(err_100)
std_err_100 = np.std(err_100)

##### Warming from FAIR vs warming from GWP*
# Relative error from each scenario and year
err_ipcc_star = relative_error(T_all_ipcc_fair[115:,:], T_ipcc_star[115:,:])
err_ssp_star = relative_error(T_ssp_fair[115:,:], T_ssp_star[115:,:])
err_ssp_star = np.delete(err_ssp_star,[3,5,7],axis=1)
err_star = np.hstack((err_ipcc_star.reshape(len(err_ipcc_star)*len(err_ipcc_star[0])),err_ssp_star.reshape(len(err_ssp_star)*len(err_ssp_star[0]))))
# Mean and standard deviation of the error
mean_err_star = np.mean(err_star)
std_err_star = np.std(err_star)

##### Warming from FAIR vs warming from GWP100
# Relative error from each scenario and year
err_ipcc_lwe = relative_error(T_all_ipcc_fair[115:,:], T_ipcc_lwe[115:,:])
err_ssp_lwe = relative_error(T_ssp_fair[115:,:], T_ssp_lwe[115:,:])
err_ssp_lwe = np.delete(err_ssp_lwe,[3,5,7],axis=1)
err_lwe = np.hstack((err_ipcc_lwe.reshape(len(err_ipcc_lwe)*len(err_ipcc_lwe[0])),err_ssp_lwe.reshape(len(err_ssp_lwe)*len(err_ssp_lwe[0]))))
# Mean and standard deviation of the error
mean_err_lwe = np.mean(err_lwe)
std_err_lwe = np.std(err_lwe)

##### Plot warming with FAIR vs warming with the methods
fig, ax = plt.subplots(2, 2, figsize=(12,9), dpi=500)
# fig.suptitle('Total warming vs expected warming from the carbon budgets')
# Effective TCRE
ax[0,0].scatter(T_all_ipcc_fair[115:,:], T_ipcc_fict[115:,:], s=2, alpha=0.05, label='All scenarios', color='tab:blue')
ax[0,0].scatter(np.delete(T_ssp_fair,[3,5,7],axis=1)[115:,:], np.delete(T_ssp_fict,[3,5,7],axis=1)[115:,:], s=2, alpha=0.05, color='tab:blue')
ax[0,0].axline((0,0), slope=1, label='y=x', color='k')
ax[0,0].set_title('a) IPCC carbon budget')
ax[0,0].set_xlabel('Warming from FAIR starting in 2015  (°C)')
ax[0,0].set_ylabel('Warming using IPCC carbon budegt starting in 2015 (°C)')
ax[0,0].text(2.5,0.5,f'Error:\nmean: {mean_err_eff:.3f}°C\nstandard deviation: {std_err_eff:.3f}°C')
ax[0,0].legend()

# GWP100
ax[0,1].scatter(T_all_ipcc_fair[115:,:], T_ipcc_100[115:,:], s=2, alpha=0.05, label='All scenarios', color='tab:blue')
ax[0,1].scatter(np.delete(T_ssp_fair,[3,5,7],axis=1)[115:,:], np.delete(T_ssp_100,[3,5,7],axis=1)[115:,:], s=2, alpha=0.05, color='tab:blue')
ax[0,1].axline((0,0), slope=1, label='y=x', color='k')
ax[0,1].set_title('b) GWP$_{100}$ metric')
ax[0,1].set_xlabel('Warming from FAIR starting in 2015  (°C)')
ax[0,1].set_ylabel('Warming using GWP$_{100}$ starting in 2015 (°C)')
ax[0,1].text(2.5,0.5,f'Error:\nmean: {mean_err_100:.3f}°C\nstandard deviation: {std_err_100:.3f}°C')
ax[0,1].legend()

# GWP*
ax[1,0].scatter(T_all_ipcc_fair[115:,:], T_ipcc_star[115:,:], s=2, alpha=0.05, label='All scenarios', color='tab:blue')
ax[1,0].scatter(np.delete(T_ssp_fair,[3,5,7],axis=1)[115:,:], np.delete(T_ssp_star,[3,5,7],axis=1)[115:,:], s=2, alpha=0.05, color='tab:blue')
ax[1,0].axline((0,0), slope=1, label='y=x', color='k')
ax[1,0].set_title('c) GWP* metric')
ax[1,0].set_xlabel('Warming from FAIR starting in 2015  (°C)')
ax[1,0].set_ylabel('Warming using GWP* starting in 2015 (°C)')
ax[1,0].text(2.5,0.5,f'Error:\nmean: {mean_err_star:.3f}°C\nstandard deviation: {std_err_star:.3f}°C')
ax[1,0].legend()

# LWE
ax[1,1].scatter(T_all_ipcc_fair[115:,:], T_ipcc_lwe[115:,:], s=2, alpha=0.05, label='All scenarios', color='tab:blue')
ax[1,1].scatter(np.delete(T_ssp_fair,[3,5,7],axis=1)[115:,:], np.delete(T_ssp_lwe,[3,5,7],axis=1)[115:,:], s=2, alpha=0.05, color='tab:blue')
ax[1,1].axline((0,0), slope=1, label='y=x', color='k')
ax[1,1].set_title('d) LWE methods')
ax[1,1].set_xlabel('Warming from FAIR starting in 2015  (°C)')
ax[1,1].set_ylabel('Warming using LWE starting in 2015 (°C)')
ax[1,1].text(2.5,0.5,f'Error:\nmean: {mean_err_lwe:.3f}°C\nstandard deviation: {std_err_lwe:.3f}°C')
ax[1,1].legend()
plt.tight_layout()
plt.show()

# =============================================================================
# Temperature error relative to IPCC AR6 carbon budget
# =============================================================================
##### Load all RGCBs from IPCC AR6
ar6_budget_quantile = pd.read_csv(path+'_None_recEm209.csv')

last_hist_year = np.where(years == 2019)[0][0] - ref_year_index
hist_dT = np.median(T_all_ipcc_fair[last_hist_year,:])

##### Calculate warming associated to each RGCB
T_ar6_budget = ar6_budget_quantile.copy()
err_ar6 = np.zeros((5,len(T_ar6_budget)))
for quantile in ar6_budget_quantile.keys()[2:]:
    T_ar6_budget[quantile] = ar6_budget_quantile[quantile]*converter*np.median(tcre_nor_fair)*1e-3/(1 - EF_avg*np.median(tcre_nor_fair)*1e-3) + ZEC + hist_dT
    err_ar6[np.where(ar6_budget_quantile.keys() == quantile)[0][0]-2] = relative_error(T_ar6_budget['dT_targets'], T_ar6_budget[quantile])
mean_err_ar6 = np.mean(err_ar6[2,:])
std_err_ar6 = np.std(err_ar6[2,:])

ax[1,1].scatter(T_ar6_budget['dT_targets'], T_ar6_budget['0.5'], s=2, alpha=0.5, label='All scenarios', color='tab:blue')
ax[1,1].axline((0,0), slope=1, label='y=x', color='k')
ax[1,1].set_title('d) AR6 IPCC carbon budget')
ax[1,1].set_xlabel('Warming from FAIR starting in 2015  (°C)')
ax[1,1].set_ylabel('Warming from AR6 IPCC after 2015 (°C)')
ax[1,1].text(1.5,0.5,f'Error:\nmean: {mean_err_ar6:.3f}\nstandard deviation: {std_err_ar6:.3f}')
ax[1,1].legend()
plt.tight_layout()
plt.show()


#%%
###############################################################################
######################## Remaining global carbon budget #######################
###############################################################################

# =============================================================================
# Using median TCRE
# =============================================================================
# Year at start of RGCB (2020 so one year before)
last_hist_year = np.where(years == 2019)[0][0] - ref_year_index
# Year at start of Kyoto Protocol (1998 so one year before)
Kyoto_prot_year = np.where(years == 1997)[0][0] - ref_year_index
# Year at start of Paris Agreement (2016 so one year before)
Paris_agr_year = np.where(years == 2015)[0][0] - ref_year_index
# Historical warming
hist_dT = np.median(T_all_ipcc_fair[last_hist_year,:])
before_Kyoto_dT = np.median(T_ssp_fair[Kyoto_prot_year,:])
before_Paris_dT = np.median(T_ssp_fair[Paris_agr_year,:])

# Calculate the Earth system feedback range
EF_range = ipcc_EF(T_target[0], EF_avg, EF_std, n_return)

##### For a 1.5°C-warming since the pre-industrial period
# The TCREs units being °C/TtCO2, we multiply by 1000 to have a RGCB in GtCO2
# RGCB with unchanged emissions
rgcb15_nor_dist_fair = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_nor_fair/1e3, EF_range)
# rgcb15_log_dist_fair = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_log_fair/1e3, EF_range)
# rgcb15_lmm_dist_fair = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_lmm_fair/1e3, EF_range)

# RGCB with GWP100
rgcb15_nor_dist_100 = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_nor_100/1e3, EF_range)
# rgcb15_log_dist_100 = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_log_100/1e3, EF_range)
# rgcb15_lmm_dist_100 = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_lmm_100/1e3, EF_range)

# RGCB with GWP*
rgcb15_nor_dist_star = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_nor_star/1e3, EF_range)
# rgcb15_log_dist_star = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_log_star/1e3, EF_range)
# rgcb15_lmm_dist_star = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_lmm_star/1e3, EF_range)

# RGCB with LWE
rgcb15_nor_dist_lwe = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_nor_lwe/1e3, EF_range)
# rgcb15_log_dist_lwe = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_log_lwe/1e3, EF_range)
# rgcb15_lmm_dist_lwe = ipcc_RGCB(T_target[0], ZEC, hist_dT, tcre_lmm_lwe/1e3, EF_range)

# RGCb by quantiles (according to AR6 IPCC)
rgcb_fair_quantiles = np.quantile(rgcb15_nor_dist_fair, 1 - quantiles)
rgcb_100_quantiles = np.quantile(rgcb15_nor_dist_100, 1 - quantiles)
rgcb_star_quantiles = np.quantile(rgcb15_nor_dist_star, 1 - quantiles)
rgcb_lwe_quantiles = np.quantile(rgcb15_nor_dist_lwe, 1 - quantiles)
                                              
# Average RGCB
rgcb15_fair_nor = np.median(rgcb15_nor_dist_fair)
rgcb15_100_nor = np.median(rgcb15_nor_dist_100)
rgcb15_star_nor = np.median(rgcb15_nor_dist_star)
rgcb15_lwe_nor = np.median(rgcb15_nor_dist_lwe)

##### Year at which this RGCB is reached (IPCC)
y_fair = np.zeros(len(cumul_all_ipcc_fair[0]))
y_100 = np.zeros(len(cumul_all_ipcc_fair[0]))
y_star = np.zeros(len(cumul_all_ipcc_fair[0]))
y_lwe = np.zeros(len(cumul_all_ipcc_fair[0]))
for i in range(len(cumul_all_ipcc_fair[0])):
    # Global carbon budget in 2015 (in GtCO2)
    cb_in_2015_fair = cumul_all_ipcc_fair[np.where(years_ipcc_and_before[ref_year_index:] == 2019)[0][0],i]
    cb_in_2015_100 = cumul_all_ipcc_100[np.where(years_ipcc_and_before[ref_year_index:] == 2019)[0][0],i]
    cb_in_2015_star = cumul_all_ipcc_star[np.where(years_ipcc_and_before[ref_year_index:] == 2019)[0][0],i]
    cb_in_2015_lwe = cumul_all_ipcc_lwe[np.where(years_ipcc_and_before[ref_year_index:] == 2019)[0][0],i]
    # Year where RGCB 1.5°C is reached 
    y_fair[i] = years[ref_year_index+np.argmax(cumul_all_ipcc_fair[:,i] > cb_in_2015_fair + rgcb15_fair_nor/converter)]
    y_100[i]= years[ref_year_index+np.argmax(cumul_all_ipcc_100[:,i] > cb_in_2015_100 + rgcb15_100_nor/converter)]
    y_star[i] = years[ref_year_index+np.argmax(cumul_all_ipcc_star[:,i] > cb_in_2015_star + rgcb15_star_nor/converter)]
    y_lwe[i] = years[ref_year_index+np.argmax(cumul_all_ipcc_lwe[:,i] > cb_in_2015_lwe + rgcb15_lwe_nor/converter)]

year_of_rgcb15 = pd.DataFrame(0,index=range(1),columns=case)
year_of_rgcb15[case[0]] = np.median(y_fair)
year_of_rgcb15[case[1]] = np.median(y_100)
year_of_rgcb15[case[2]] = np.median(y_star)
year_of_rgcb15[case[3]] = np.median(y_lwe)

##### RGCB starting from different years 
# RGCB starting in 1998 (after start of Kyoto Protocol)
rgcb15_nor_dist_fair_Kyoto = ipcc_RGCB(T_target[0], ZEC, before_Kyoto_dT, tcre_nor_fair/1e3, EF_range)
rgcb15_nor_dist_lwe_Kyoto = ipcc_RGCB(T_target[0], ZEC, before_Kyoto_dT, tcre_nor_lwe/1e3, EF_range)
rgcb15_fair_nor_Kyoto = np.median(rgcb15_nor_dist_fair_Kyoto)
rgcb15_lwe_nor_Kyoto = np.median(rgcb15_nor_dist_lwe_Kyoto)

# only for unchanged CO2 emissions and CO2-eq. emissions converted with LWE

# RGCB starting in 2016 (after start of Paris Agreement)
rgcb15_nor_dist_fair_Paris = ipcc_RGCB(T_target[0], ZEC, before_Kyoto_dT, tcre_nor_fair/1e3, EF_range)
rgcb15_nor_dist_lwe_Paris = ipcc_RGCB(T_target[0], ZEC, before_Kyoto_dT, tcre_nor_lwe/1e3, EF_range)
rgcb15_fair_nor_Paris = np.median(rgcb15_nor_dist_fair_Kyoto)
rgcb15_lwe_nor_Paris = np.median(rgcb15_nor_dist_lwe_Kyoto)



