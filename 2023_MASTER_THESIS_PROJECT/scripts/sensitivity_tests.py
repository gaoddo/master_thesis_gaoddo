#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 15:48:23 2023

@author: gabrieleoddo
"""

# Import modules

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# Import functions
from thesis_functions import tcre_calculation

# IPCC functions
from thesis_functions import ipcc_tcre_distribution
from thesis_functions import ipcc_RGCB
from thesis_functions import ipcc_EF


#%%
###############################################################################
########################## Initial variables and data #########################
###############################################################################

##### Directory
path = '/Users/gabrieleoddo/Documents/ETHZ/Master_Thesis/FaIR_Gabriele/'

##### Lists of names with the climate forcers and pathway
# Different cases
case = ['True','GWP100','GWPstar','LWE']
# Climate forcers emissions considered + year
names = ['Year','FossilCO2','OtherCO2','CH4','N2O','SOx','CO','NMVOC','NOx','BC','OC','NH3','CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','CARB_TET','MCF','HCFC_22','HCFC_141B','HCFC_142B','HALON1211','HALON1202','HALON1301','HALON2402','CH3BR','CH3CL']
# Non-greenhouse gases
non_ghg_names = ['SOx','NMVOC','NOx','BC','OC','NH3', 'CO']
# Greenhouse gases (non-CO2)
other_ghg_names = sorted(list(set(names[3:]) - set(non_ghg_names)))
# Indirect greenhouse gases
indirect_ghg = ['BC','CO','NH3','NMVOC','NOx']
# Cooling impact climate forcers
neg_gwp_gases = ['OC','SOx']
# F-gases names
F_gases = ['CF4','C2F6','C6F14','HFC23','HFC32','HFC43_10','HFC125','HFC134a','HFC143a','HFC227ea','HFC245fa','SF6','CFC_11','CFC_12','CFC_113','CFC_114','CFC_115','HCFC_22','HCFC_141B','HCFC_142B']
# Socioeconomic pathways considered
ssp = ['ssp119','ssp126','ssp245','ssp370','ssp434','ssp460','ssp534','ssp585']

##### Unit conversions
# Exact unit
units = ['','GtC/yr','GtC/yr','MtCH4/yr','MtN2O-N/yr','MtS/yr','MtCO/yr','Mt/yr','MtN/yr','Mt/yr','Mt/yr','MtN/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr','kt/yr']
# Convert in t of emisssion
unit_ton = [1e9,1e9,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e6,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3]
# Convert from GtC to GtCO2
converter = 44.01/12.011 

##### Parameters for the RGCB
# Quantiles of probability
quantiles = np.array([0.17, 0.33, 0.5, 0.66, 0.83])
# Zero emissions commitment (warming after net-zero emissions)
ZEC = 0

##### Data from calculate_rgcb.py
# Sensitivity test 1: simulations done since 2015 (historical emissions disregarded)
T_fair_from15 = np.array(pd.read_csv(path+'from_rgcb_for_ssp/T_all_ipcc_fair_from15.csv'))[:,1:]
cumul_fair_from15 = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_fair_from15.csv'))[:,1:]
cumul_100_from15 = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_100_from15.csv'))[:,1:]
cumul_star_from15 = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_star_from15.csv'))[:,1:]
cumul_lwe_from15 = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_lwe_from15.csv'))[:,1:]

# Sensitivity test 2: each climate forcer different than CO2, CH4 and N2O disregarded
T_fair_simp = np.array(pd.read_csv(path+'from_rgcb_for_ssp/T_all_ipcc_fair_simp.csv'))[:,1:]
cumul_fair_simp = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_fair_simp.csv'))[:,1:]
cumul_100_simp = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_100_simp.csv'))[:,1:]
cumul_star_simp = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_star_simp.csv'))[:,1:]
cumul_lwe_simp = np.array(pd.read_csv(path+'from_rgcb_for_ssp/cumul_all_ipcc_lwe_simp.csv'))[:,1:]

##### Other global variables
# All considered years (from 1765 to 2100)
years = np.arange(1765,2101)
# First industrial year, reference for the temperature change
ref_year_index = np.where(years == 1900)[0][0]

##### Average over each scenario
# Cumulative CO2(-eq) emissions median of each scenario 
cumul_avg_fair_from15 = np.median(cumul_fair_from15, axis=1)
cumul_avg_100_from15 = np.median(cumul_100_from15, axis=1)
cumul_avg_star_from5 = np.median(cumul_star_from15, axis=1)
cumul_avg_lwe_from15 = np.median(cumul_lwe_from15, axis=1)

cumul_avg_fair_simp = np.median(cumul_fair_simp, axis=1)
cumul_avg_100_simp = np.median(cumul_100_simp, axis=1)
cumul_avg_star_simp = np.median(cumul_star_simp, axis=1)
cumul_avg_lwe_simp = np.median(cumul_lwe_simp, axis=1)

##### RGCB from the reference state taken from calculate_rgcb.py (GtCO2)
rgcb15_fair_quantiles_ref = np.array([745.340218, 578.547311, 464.006112, 366.717900, 257.522312])
rgcb15_100_quantiles_ref = np.array([1126.533326, 832.533015, 668.267443, 547.735610, 409.485726])
rgcb15_star_quantiles_ref = np.array([1015.600493, 752.801970, 604.009070, 491.225879, 362.896030])
rgcb15_lwe_quantiles_ref = np.array([1028.211595, 798.974149, 653.165549, 542.894844, 414.493978])


#%%
###############################################################################
################### Simulation without historical emissions ###################
###############################################################################

# =============================================================================
# TCRE calculation
# =============================================================================
# Reshape temperature change and cumulative emissions
T_1d_fair_from15 = T_fair_from15.reshape(len(T_fair_from15)*len(T_fair_from15[0]))
cumul_1d_fair_from15 = cumul_fair_from15.reshape(len(cumul_fair_from15)*len(cumul_fair_from15[0]))
cumul_1d_100_from15 = cumul_100_from15.reshape(len(cumul_fair_from15)*len(cumul_fair_from15[0]))
cumul_1d_star_from15 = cumul_star_from15.reshape(len(cumul_fair_from15)*len(cumul_fair_from15[0]))
cumul_1d_lwe_from15 = cumul_lwe_from15.reshape(len(cumul_fair_from15)*len(cumul_fair_from15[0]))
# Put together cumulative emissions and temperature change
TC_fair_from15 = np.vstack((cumul_1d_fair_from15,T_1d_fair_from15)).T
TC_100_from15 = np.vstack((cumul_1d_100_from15,T_1d_fair_from15)).T
TC_star_from15 = np.vstack((cumul_1d_star_from15,T_1d_fair_from15)).T
TC_lwe_from15 = np.vstack((cumul_1d_lwe_from15,T_1d_fair_from15)).T

# Delete outliers (negative values in temperature change column)
TC_fair_from15 = np.delete(TC_fair_from15, np.where(TC_fair_from15[:,1] < 0)[0], 0)
TC_100_from15 = np.delete(TC_100_from15, np.where(TC_100_from15[:,1] < 0)[0], 0)
TC_star_from15 = np.delete(TC_star_from15, np.where(TC_star_from15[:,1] < 0)[0], 0)
TC_lwe_from15 = np.delete(TC_lwe_from15, np.where(TC_lwe_from15[:,1] < 0)[0], 0)

##### TCRE calculation for unchanged and converted emissions
# Low and high TCRE boundaries (in °C per GtCO2)
extrema = np.array([0.15,0.95])

tcre_quantile_fair_from15 = tcre_calculation(TC_fair_from15, np.array([0.45,1.25]))
tcre_quantile_100_from15 = tcre_calculation(TC_100_from15, extrema)
tcre_quantile_star_from15 = tcre_calculation(TC_star_from15, extrema)
tcre_quantile_lwe_from15 = tcre_calculation(TC_lwe_from15, extrema)

##### Plot TCRE of each case
# Unchanged emissions
plt.scatter(TC_fair_from15[:,0]*converter, TC_fair_from15[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_fair_from15[:,0]*converter)),np.linspace(0,np.max(TC_fair_from15[:,0]*converter))*tcre_quantile_fair_from15['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_fair_from15['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_fair_from15[:,0]*converter))*tcre_quantile_fair_from15['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_fair_from15[:,0]*converter))*tcre_quantile_fair_from15['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_fair_from15[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_fair_from15['0.05'].loc[0][0]:.3f}-{tcre_quantile_fair_from15['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for unchanged emissions (°C/TtCO$_2$) \n Emissions starting in 2015')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

plt.scatter(TC_100_from15[:,0]*converter, TC_100_from15[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_100_from15[:,0]*converter)),np.linspace(0,np.max(TC_100_from15[:,0]*converter))*tcre_quantile_100_from15['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_100_from15['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_100_from15[:,0]*converter))*tcre_quantile_100_from15['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_100_from15[:,0]*converter))*tcre_quantile_100_from15['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_100_from15[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_100_from15['0.05'].loc[0][0]:.3f}-{tcre_quantile_100_from15['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for GWP$_{100}$-converted emissions (°C/TtCO$_2$) \n Emissions starting in 2015')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

plt.scatter(TC_star_from15[:,0]*converter, TC_star_from15[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_star_from15[:,0]*converter)),np.linspace(0,np.max(TC_star_from15[:,0]*converter))*tcre_quantile_star_from15['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_star_from15['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_star_from15[:,0]*converter))*tcre_quantile_star_from15['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_star_from15[:,0]*converter))*tcre_quantile_star_from15['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_star_from15[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_star_from15['0.05'].loc[0][0]:.3f}-{tcre_quantile_star_from15['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for GWP*-converted emissions (°C/TtCO$_2$) \n Emissions starting in 2015')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

plt.scatter(TC_lwe_from15[:,0]*converter, TC_lwe_from15[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_lwe_from15[:,0]*converter)),np.linspace(0,np.max(TC_lwe_from15[:,0]*converter))*tcre_quantile_lwe_from15['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_lwe_from15['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_lwe_from15[:,0]*converter))*tcre_quantile_lwe_from15['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_lwe_from15[:,0]*converter))*tcre_quantile_lwe_from15['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_lwe_from15[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_lwe_from15['0.05'].loc[0][0]:.3f}-{tcre_quantile_lwe_from15['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for LWE-converted emissions (°C/TtCO$_2$) \n Emissions starting in 2015')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

# =============================================================================
# RGCB calculation
# =============================================================================
##### Plot TCRE distribution using IPCC functions
# Probability that results fit between the low and high value of TCRE
likelihood = 0.6827
# All possible distributions
tcre_dist = ['normal','lognormal','lognormal mean match']
# In this case we only calculate for the normal distribution
n_return = 10000

# TCRE distribution for unchanged emissions
low = tcre_quantile_fair_from15['0.05'].loc[0][0]
high = tcre_quantile_fair_from15['0.95'].loc[0][0]
tcre_nor_fair_from15 = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

# TCRE distribution for GWP100
low = tcre_quantile_100_from15['0.05'].loc[0][0]
high = tcre_quantile_100_from15['0.95'].loc[0][0]
tcre_nor_100_from15 = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

# TCRE distribution for GWP*
low = tcre_quantile_star_from15['0.05'].loc[0][0]
high = tcre_quantile_star_from15['0.95'].loc[0][0]
tcre_nor_star_from15 = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

# TCRE distribution for LWE
low = tcre_quantile_lwe_from15['0.05'].loc[0][0]
high = tcre_quantile_lwe_from15['0.95'].loc[0][0]
tcre_nor_lwe_from15 = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

##### Other parameters
# Last historical year (2019)
last_hist_year = 5
# Temperature change in 2014 (from normal case)
T_2014 = 1.0356
# Historical warming
hist_dT_from15 = np.median(T_fair_from15[last_hist_year,:]) + T_2014
# Temperature targets for the remaining global carbon budget
T_target = np.array([1.5,1.8,2.0])
# Earth climate feedback (from AR6 IPCC) in GtCO2/°C
EF_avg = 7.1*converter
EF_std = 26.7*converter
EF_range = ipcc_EF(T_target[0], EF_avg, EF_std, n_return)

##### RGCB 1.5°C warming
rgcb15_nor_dist_fair_from15 = ipcc_RGCB(T_target[0], ZEC, hist_dT_from15, tcre_nor_fair_from15/1e3, EF_range)
rgcb15_nor_dist_100_from15 = ipcc_RGCB(T_target[0], ZEC, hist_dT_from15, tcre_nor_100_from15/1e3, EF_range)
rgcb15_nor_dist_star_from15 = ipcc_RGCB(T_target[0], ZEC, hist_dT_from15, tcre_nor_star_from15/1e3, EF_range)
rgcb15_nor_dist_lwe_from15 = ipcc_RGCB(T_target[0], ZEC, hist_dT_from15, tcre_nor_lwe_from15/1e3, EF_range)

rgcb15_fair_quantiles_from15 = np.quantile(rgcb15_nor_dist_fair_from15, 1 - quantiles)
rgcb15_100_quantiles_from15 = np.quantile(rgcb15_nor_dist_100_from15, 1 - quantiles)
rgcb15_star_quantiles_from15 = np.quantile(rgcb15_nor_dist_star_from15, 1 - quantiles)
rgcb15_lwe_quantiles_from15 = np.quantile(rgcb15_nor_dist_lwe_from15, 1 - quantiles)

#%%
###############################################################################
##################### Simulation only with CO2, CH4, N2O ######################
###############################################################################

# =============================================================================
# TCRE calculation
# =============================================================================
# Reshape temperature change and cumulative emissions
T_1d_fair_simp = T_fair_simp.reshape(len(T_fair_simp)*len(T_fair_simp[0]))
cumul_1d_fair_simp = cumul_fair_simp.reshape(len(cumul_fair_simp)*len(cumul_fair_simp[0]))
cumul_1d_100_simp = cumul_100_simp.reshape(len(cumul_fair_simp)*len(cumul_fair_simp[0]))
cumul_1d_star_simp = cumul_star_simp.reshape(len(cumul_fair_simp)*len(cumul_fair_simp[0]))
cumul_1d_lwe_simp = cumul_lwe_simp.reshape(len(cumul_fair_simp)*len(cumul_fair_simp[0]))
# Put together cumulative emissions and temperature change
TC_fair_simp = np.vstack((cumul_1d_fair_simp,T_1d_fair_simp)).T
TC_100_simp = np.vstack((cumul_1d_100_simp,T_1d_fair_simp)).T
TC_star_simp = np.vstack((cumul_1d_star_simp,T_1d_fair_simp)).T
TC_lwe_simp = np.vstack((cumul_1d_lwe_simp,T_1d_fair_simp)).T

# Delete outliers (negative values in temperature change column)
TC_fair_simp = np.delete(TC_fair_simp, np.where(TC_fair_simp[:,1] < 0)[0], 0)
TC_100_simp = np.delete(TC_100_simp, np.where(TC_100_simp[:,1] < 0)[0], 0)
TC_star_simp = np.delete(TC_star_simp, np.where(TC_star_simp[:,1] < 0)[0], 0)
TC_lwe_simp = np.delete(TC_lwe_simp, np.where(TC_lwe_simp[:,1] < 0)[0], 0)

##### TCRE calculation for unchanged and converted emisisons
# Low and high TCRE boundaries (in °C per GtCO2)
extrema = np.array([0.15,0.95])

tcre_quantile_fair_simp = tcre_calculation(TC_fair_simp, np.array([0.45,1.25]))
tcre_quantile_100_simp = tcre_calculation(TC_100_simp, extrema)
tcre_quantile_star_simp = tcre_calculation(TC_star_simp, extrema)
tcre_quantile_lwe_simp = tcre_calculation(TC_lwe_simp, extrema)

##### Plot TCRE of each case
# Unchanged emissions
plt.scatter(TC_fair_simp[:,0]*converter, TC_fair_simp[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_fair_simp[:,0]*converter)),np.linspace(0,np.max(TC_fair_simp[:,0]*converter))*tcre_quantile_fair_simp['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_fair_simp['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_fair_simp[:,0]*converter))*tcre_quantile_fair_simp['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_fair_simp[:,0]*converter))*tcre_quantile_fair_simp['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_fair_simp[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_fair_simp['0.05'].loc[0][0]:.3f}-{tcre_quantile_fair_simp['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for unchanged emissions (°C/TtCO$_2$) \n Emissions of only CO$_2$, CH$_4$ and N$_2$O')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

plt.scatter(TC_100_simp[:,0]*converter, TC_100_simp[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_100_simp[:,0]*converter)),np.linspace(0,np.max(TC_100_simp[:,0]*converter))*tcre_quantile_100_simp['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_100_simp['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_100_simp[:,0]*converter))*tcre_quantile_100_simp['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_100_simp[:,0]*converter))*tcre_quantile_100_simp['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_100_simp[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_100_simp['0.05'].loc[0][0]:.3f}-{tcre_quantile_100_simp['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for GWP$_{100}$-converted emissions (°C/TtCO$_2$) \n Emissions of only CO$_2$, CH$_4$ and N$_2$O')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

plt.scatter(TC_star_simp[:,0]*converter, TC_star_simp[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_star_simp[:,0]*converter)),np.linspace(0,np.max(TC_star_simp[:,0]*converter))*tcre_quantile_star_simp['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_star_simp['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_star_simp[:,0]*converter))*tcre_quantile_star_simp['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_star_simp[:,0]*converter))*tcre_quantile_star_simp['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_star_simp[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_star_simp['0.05'].loc[0][0]:.3f}-{tcre_quantile_star_simp['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for GWP*-converted emissions (°C/TtCO$_2$) \n Emissions of only CO$_2$, CH$_4$ and N$_2$O')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

plt.scatter(TC_lwe_simp[:,0]*converter, TC_lwe_simp[:,1], alpha=0.05, s=2, label='All scenarios', color='tab:blue')
plt.plot(np.linspace(0,np.max(TC_lwe_simp[:,0]*converter)),np.linspace(0,np.max(TC_lwe_simp[:,0]*converter))*tcre_quantile_lwe_simp['0.5'].loc[0][0]/1e3, label=f"TCRE={tcre_quantile_lwe_simp['0.5'].loc[0][0]:.3f} (median value)", color='red')
low = np.linspace(0,np.max(TC_lwe_simp[:,0]*converter))*tcre_quantile_lwe_simp['0.05'].loc[0][0]/1e3
high = np.linspace(0,np.max(TC_lwe_simp[:,0]*converter))*tcre_quantile_lwe_simp['0.95'].loc[0][0]/1e3
plt.fill_between(np.linspace(0,np.max(TC_lwe_simp[:,0]*converter)),high,low, label=f"5-95% range of {tcre_quantile_lwe_simp['0.05'].loc[0][0]:.3f}-{tcre_quantile_lwe_simp['0.95'].loc[0][0]:.3f}", color='green', alpha=0.2)
plt.title('TCRE range for LWE-converted emissions (°C/TtCO$_2$) \n Emissions of only CO$_2$, CH$_4$ and N$_2$O')
plt.xlabel('Cumulative CO$_2$ emissions (GtCO$_2$)')
plt.ylabel('Temperature change relative to 1900 (°C)')
plt.legend()
plt.rcParams["figure.dpi"] = 500
plt.show()

# =============================================================================
# RGCB calculation
# =============================================================================
##### Plot TCRE distribution using IPCC functions (only with normal distribution)
# TCRE distribution for unchanged emissions
low = tcre_quantile_fair_simp['0.05'].loc[0][0]
high = tcre_quantile_fair_simp['0.95'].loc[0][0]
tcre_nor_fair_simp = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

# TCRE distribution for GWP100
low = tcre_quantile_100_simp['0.05'].loc[0][0]
high = tcre_quantile_100_simp['0.95'].loc[0][0]
tcre_nor_100_simp = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

# TCRE distribution for GWP*
low = tcre_quantile_star_simp['0.05'].loc[0][0]
high = tcre_quantile_star_simp['0.95'].loc[0][0]
tcre_nor_star_simp = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

# TCRE distribution for LWE
low = tcre_quantile_lwe_simp['0.05'].loc[0][0]
high = tcre_quantile_lwe_simp['0.95'].loc[0][0]
tcre_nor_lwe_simp = ipcc_tcre_distribution(low, high, likelihood, n_return, tcre_dist[0])

# Last historical year (2019)
last_hist_year = np.where(years == 2019)[0][0] - ref_year_index
# Historical warming
hist_dT_simp = np.median(T_fair_simp[last_hist_year,:])

##### RGCB 1.5°C warming
rgcb15_nor_dist_fair_simp = ipcc_RGCB(T_target[0], ZEC, hist_dT_simp, tcre_nor_fair_simp/1e3, EF_range)
rgcb15_nor_dist_100_simp = ipcc_RGCB(T_target[0], ZEC, hist_dT_simp, tcre_nor_100_simp/1e3, EF_range)
rgcb15_nor_dist_star_simp = ipcc_RGCB(T_target[0], ZEC, hist_dT_simp, tcre_nor_star_simp/1e3, EF_range)
rgcb15_nor_dist_lwe_simp = ipcc_RGCB(T_target[0], ZEC, hist_dT_simp, tcre_nor_lwe_simp/1e3, EF_range)

rgcb15_fair_quantiles_simp = np.quantile(rgcb15_nor_dist_fair_simp, 1 - quantiles)
rgcb15_100_quantiles_simp = np.quantile(rgcb15_nor_dist_100_simp, 1 - quantiles)
rgcb15_star_quantiles_simp = np.quantile(rgcb15_nor_dist_star_simp, 1 - quantiles)
rgcb15_lwe_quantiles_simp = np.quantile(rgcb15_nor_dist_lwe_simp, 1 - quantiles)

# =============================================================================
# Plot RGCB of all cases for a better visualization
# =============================================================================
plt.scatter(np.ones(5), rgcb15_fair_quantiles_ref, label='Reference case', color='tab:blue')
plt.scatter(1.1*np.ones(5), rgcb15_fair_quantiles_from15, label='No historical emissions', color='tab:orange')
plt.scatter(1.2*np.ones(5), rgcb15_fair_quantiles_simp, label='Only CO$_2$, CH$_4$ and N$_2$O', color='tab:green')

plt.scatter(2*np.ones(5), rgcb15_100_quantiles_ref, color='tab:blue')
plt.scatter(2.1*np.ones(5), rgcb15_100_quantiles_from15, color='tab:orange')
plt.scatter(2.2*np.ones(5), rgcb15_100_quantiles_simp, color='tab:green')

plt.scatter(3*np.ones(5), rgcb15_star_quantiles_ref, color='tab:blue')
plt.scatter(3.1*np.ones(5), rgcb15_star_quantiles_from15, color='tab:orange')
plt.scatter(3.2*np.ones(5), rgcb15_star_quantiles_simp, color='tab:green')

plt.scatter(4*np.ones(5), rgcb15_lwe_quantiles_ref, color='tab:blue')
plt.scatter(4.1*np.ones(5), rgcb15_lwe_quantiles_from15, color='tab:orange')
plt.scatter(4.2*np.ones(5), rgcb15_lwe_quantiles_simp, color='tab:green')

plt.title('RGCB for different cases')
plt.xticks([1.1,2.1,3.1,4.1], ['Uchanged','GWP$_{100}$','GWP*','LWE'])
plt.ylabel('Cumulative CO$_2$/CO$_2$-eq. emissions')
plt.legend()

data = [
    [rgcb15_fair_quantiles_ref, rgcb15_fair_quantiles_from15, rgcb15_fair_quantiles_simp],
    [rgcb15_100_quantiles_ref, rgcb15_100_quantiles_from15, rgcb15_100_quantiles_simp],
    [rgcb15_star_quantiles_ref, rgcb15_star_quantiles_from15, rgcb15_star_quantiles_simp],
    [rgcb15_lwe_quantiles_ref, rgcb15_lwe_quantiles_from15, rgcb15_lwe_quantiles_simp]
]

fig, ax = plt.subplots()
# Labels and positions for the x-axis
labels = ['Reference case', 'No historical\nemissions', 'Only CO$_2$, \nCH$_4$, and N$_2$O']
positions = [1, 2, 3]
# Colors for the boxplots
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red']

# Create boxplots for each case
boxplots = []
for i, case_data in enumerate(data):
    box = ax.boxplot(case_data, positions=[pos + i * 0.2 for pos in positions], widths=0.18, medianprops = dict(color = "black", linewidth = 1.5), patch_artist=True)
    boxplots.append(box)

    # Set box colors
    for patch in box['boxes']:
        patch.set_facecolor(colors[i])

# Set the title and labels
plt.title('RGCB for different cases')
plt.ylabel('Cumulative CO$_2$/CO$_2$-eq. emissions (GtCO$_2$)')

# Set x-axis ticks and labels
ax.set_xticks([pos + 0.3 for pos in positions])
ax.set_xticklabels(labels)

# Show legend
handles = [plt.Rectangle((0, 0), 1, 1, color=color) for color in colors]
plt.legend(handles, ['Unchanged','GWP$_{100}$','GWP*','LWE'], loc='upper right')
plt.show()

