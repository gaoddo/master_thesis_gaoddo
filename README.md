### Master's Thesis Project

This project aims to calculate the remaining global carbon budget (RGCB) using non-CO2 warming-equivalent methods, which convert emissions of greenhouse gases and other climate forcers into CO2-eq. emissions. From this RGCB, the Swiss remaining carbon budget is calculated based on different allocation mechansims and then compared to a budget arising from depleting pathways consistent with Switzerland's long-term strategy.

### Getting started

The dataset containing the majority of the emissions scenarios (1683) used in this work is found on the AR6 IPCC Database.

The model used (FAIR) is directly integrated in a Python Library and includes 8 SSP scenarios, completing the scenario database of this work.

### Usage

lwe_parameters.py and thesis_functions.py are scripts required for the other scripts.

ssp_CO2eq.py converts the 8 SSP scenarios into CO2-eq. emissions, which are then used in calculate_rgcb.py. The latter reproduces the same conversions as the ssp_CO2eq.py script, but for the other 1683 scenarios. It then provides plots of TCRE (one for each global carbon budget), before giving the quantification of the RGCB for unchanged CO2 emissions and for CO2-eq. emissions.

CH_budget.py constructs different allocation mechansims depending on worldwide population and emissions, then uses the results of calculate_rgcb.py to determine different Swiss remaining carbon/carbon-eq. budget depending on the different allocation mechanisms. Moreover it determines the year when these budgets are depleted, following a depleting pathway and compares everything to Switzerland's long-term climate strategy.

pulse.py gives a visualization of pulse and sustained emissions of a single greenhouse gas, in this case CH4 and N2O.

sensitivity_test.py compares the RGCB calculated in calculate_rgcb.py with a RGCB which comes from emissions disregarding historical emissions or from emissions considering only CO2, CH4 and N2O.
